package com.example.xignale.Utilidades;

public class Constantes
{
    public class BundleKey {
        //dialogo
        public final static String FONDO_KEY = "fondoKey";
        public final static String TITULO_KEY = "tituloKey";
        public final static String DESCRIPCION_KEY = "descripcionKey";
        public final static String IMAGEN_KEY = "imagenKey";
        public final static String BTN_GRANDE_KEY = "btnGrandeKey";
        public static final String BTN_PEQUENIO_KEY = "btnPequenioKey";

        public final static String IDENTIFICADOR_USUARIO = "identificadorUsuario";
        public final static String NOMBRES_USUARIO = "nombreUsuario";
        public final static String APELLIDOS_USUARIO = "apellidosUsuario";
        public final static String CORREO_USUARIO = "correoUsuario";
        public final static String FOTO_USUARIO = "fotoUsuario";
        public final static String CELULAR_USUARIO = "celularUsuario";
        public final static String CEDULA_USUARIO = "cedulaUsuario";
        public final static String EMPRESA_USUARIO = "empresaUsuario";

        public final static String VERIFICAR_FORM = "form";
        public final static String FORM_INCOMPLETO = "formInc";

        public final static String TIPO_ALERTA = "tipoAlerta";
        public final static String DESCRIPCION_ALERTA = "descripcionAlerta";
        public final static String LUGAR_ALERTA = "lugarAlerta";
        public final static String FECHA_ALERTA = "fechaAlerta";

        public final static String URL_MAPA = "urlMapa";

        //Tienda
        public final static String REQUEST_CODE = "requestCode";
        public final static String RESULT_CODE = "resultCode";

    }

    public class Fallas {
        public final static String ERROR_RECUPERACION_CONTRA = "errorRecContra";
        public final static String NO_INTERNET = "NoInternet";
        public final static String FALTA_REGISTRO = "faltaRegistro";
        public final static String CEL_YA_REGISTRADO = "celYaRegistrado";
        public final static String CEL_INCORRECTO = "celIncorrecto";
        public final static String EXCESO_SOLICITUES_CEL = "muchasSolicitidesCel";
        public final static String INFO_INCORRECTA_CORREO = "informacionPersonalIncorrecta";
        public final static String ERROR_REGISTRO_FACEBOOK = "errorFacebook";
        public final static String ERROR_REGISTRO_GOOGLE = "errorGoogle";
        public final static String FALLO_SUBIENDO_FOTO = "uploadError";
        public final static String ERROR_LISTA_CATEGORIAS = "errorCategorias";
        public final static String ERROR_IMAGENES_PROMOCIONES_DASH = "errorPromocionesDash";
        public final static String ERROR_NUMERO_REFERENCIA = "errorNumeroReferencia";
    }

    public class Storage {
        public final static String APLICACION = "aplicacion";
        public final static String PROMOCIONES = "listaPromociones";
    }

    public class SharedPreferences {
        public final static String DOC_USER_PREFERENCES = "docUsuario";
        public final static String USER = "usuario";
        public final static String VACIO = "";
        public final static String FOTO_VACIA = "https://xignalE.co";
    }

    public class Servicios
    {
        public final static String AGREGAR_ALERTA = "agregarAlerta";
        public final static String MOSTRAR_INFO_USUARIO = "mostrarInfoUsuario";
        public final static String AGREGAR_USUARIO = "agregarUsuario";
    }

    public class Colecciones
    {
        public final static String USUARIOS = "usuarios";
        public final static String ALERTAS = "alertas";
    }
}

