package com.example.xignale;

import android.app.Application;

import io.mapwize.mapwizeformapbox.AccountManager;

public class PrincipalMapaActivity extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        AccountManager.start(this, "f4d025b2bdd223e0e8584217fa64da67");
    }

}
