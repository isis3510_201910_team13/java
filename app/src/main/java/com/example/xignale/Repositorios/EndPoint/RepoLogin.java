package com.example.xignale.Repositorios.EndPoint;

import android.content.Context;

import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarseListener;
import com.example.xignale.Repositorios.Proxy.FirebaseFireStoreDB;
import com.example.xignale.Utilidades.Constantes;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Map;
import java.util.TreeMap;

public class RepoLogin
{
    private Context activity;
    private String TAG= "repoLogin";
    private FirebaseFireStoreDB firebase;

    public RepoLogin (Context activity)
    {
        this.activity = activity;
        this.firebase = new FirebaseFireStoreDB(activity);
    }

    public void guardarUsuario(String apellidos, String cedula, String celular, String correo, String empresa, String nombres, final IProductionListener listener, final String etiqueta)
    {
        TreeMap<String, String> coleccion = new TreeMap<>();
        coleccion.put(Constantes.Colecciones.USUARIOS,FirebaseAuth.getInstance().getUid());
        Map<String,Object> usuariosDoc = new TreeMap<>();
        usuariosDoc.put("apellidos", apellidos);
        usuariosDoc.put("cedula", cedula);
        usuariosDoc.put("celular", celular);
        usuariosDoc.put("correo", correo);
        usuariosDoc.put("empresaAsociada",empresa);
        usuariosDoc.put("nombres", nombres);
        usuariosDoc.put("urlFotoPerfil","https://firebasestorage.googleapis.com/v0/b/xignal-ee4fa.appspot.com/o/waldoo.png?alt=media&token=15ed732b-e52d-4cc0-9764-6b37983367e2");
        firebase.agregarDocumento(coleccion, usuariosDoc, listener, etiqueta, Usuario.class);

    }
    //private FirebaseAutentication autenticacion;

}
