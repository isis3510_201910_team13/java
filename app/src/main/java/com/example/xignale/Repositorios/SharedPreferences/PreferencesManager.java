package com.example.xignale.Repositorios.SharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Utilidades.Constantes;
import com.google.gson.Gson;

public class PreferencesManager
{
    public static SharedPreferences getDocUserSharedPreferences(Context context) {
        return context.getSharedPreferences(Constantes.SharedPreferences.DOC_USER_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static void setUser(Usuario user, Context context) {
        Gson gson = new Gson();
        String usuario = gson.toJson(user);
        SharedPreferences.Editor editor = getDocUserSharedPreferences(context).edit();
        editor.putString(Constantes.SharedPreferences.USER, usuario);
        editor.apply();
    }

    public static Usuario getUser(Context context) {
        Gson gson = new Gson();
        String usuarioShared = getDocUserSharedPreferences(context).getString(Constantes.SharedPreferences.USER, Constantes.SharedPreferences.VACIO);
        if (usuarioShared.equals(Constantes.SharedPreferences.VACIO)) {
            return null;
        } else
            return gson.fromJson(usuarioShared, Usuario.class);
    }
}
