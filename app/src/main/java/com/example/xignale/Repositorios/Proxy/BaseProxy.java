package com.example.xignale.Repositorios.Proxy;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;

import com.example.xignale.Base.BaseModel;
import com.example.xignale.Repositorios.EndPoint.IProductionListener;

public class BaseProxy
{
    //Método que vaya verificando la conexión a Internet constantemente

    protected Context context;

    public BaseProxy(Context context)
    {
        this.context =context;
    }

    public boolean conexionInternet()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            return true;
        }
        return false;

    }

    protected <T extends BaseModel>void actualizacionSincronica(final IProductionListener listener, final T user, final String etiqueta){
        ((FragmentActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listener.actualizacion(etiqueta, user);
            }
        });
    }

    protected <T extends BaseModel>void exitoSincronico(final IProductionListener listener, final T user, final String etiqueta){
        ((FragmentActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listener.exito(etiqueta, user);
            }
        });
    }

    protected void fallaSincronica(final IProductionListener listener, final String etiqueta){
        ((FragmentActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listener.falla(etiqueta);
            }
        });
    }
}
