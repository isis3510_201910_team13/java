package com.example.xignale.Repositorios.EndPoint;

import android.content.Context;

import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Repositorios.Proxy.FirebaseFireStoreDB;
import com.example.xignale.Utilidades.Constantes;
import com.google.firebase.Timestamp;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class RepoAlerta
{
    private Context activity;
    private String TAG = "repoAlerta";
    private FirebaseFireStoreDB firebase;

    public RepoAlerta(Context activity)
    {
        this.activity = activity;
        this.firebase = new FirebaseFireStoreDB(activity);
    }

    public void agregarAlerta(String cliente, Alerta alerta, final IProductionListener listener, final String etiqueta)
    {
        TreeMap<String, String> coleccion = new TreeMap<>();
        coleccion.put(Constantes.Colecciones.ALERTAS,alerta.uidDocumento);
        Map<String,Object> alertaDoc = new TreeMap<>();
        alertaDoc.put("fechaAlerta",alerta.getFechaAlerta());
        alertaDoc.put("descripcion", alerta.getDescripcion());
        alertaDoc.put("idUsuario", cliente);
        alertaDoc.put("lugar", alerta.getLugar());
        alertaDoc.put("tipo", alerta.getTipo());
        firebase.agregarDocumentoAutoID(coleccion, alertaDoc, listener, etiqueta, Alerta.class);
        //firebase.agregarDocumento(coleccion,alertaDoc,listener,etiqueta,alerta);
    }
}
