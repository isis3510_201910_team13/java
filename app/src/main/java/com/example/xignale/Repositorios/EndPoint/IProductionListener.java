package com.example.xignale.Repositorios.EndPoint;

public interface IProductionListener
{
    void exito(String etiqueta, Object objeto);
    void falla(String etiqueta);
    void actualizacion(String etiqueta, Object objeto);
}
