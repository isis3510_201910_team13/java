package com.example.xignale.Repositorios.Proxy;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.xignale.Base.BaseModel;
import com.example.xignale.Repositorios.EndPoint.IProductionListener;
import com.example.xignale.Utilidades.Constantes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Nullable;

public class FirebaseFireStoreDB extends BaseProxy
{
    private FirebaseFirestore firebase;

    private ArrayList<String> documentos = new ArrayList<String>();

    private ArrayList<String> campos = new ArrayList<String>();

    private ArrayList<String> colecciones = new ArrayList<String>();

    private Boolean llego = false;

    private DocumentSnapshot documentoLlegada;

    private enum constantesDB {
        documentos, colecciones, campos
    }

    public FirebaseFireStoreDB(Context context) {
        super(context);
        this.context = context;
        this.firebase = FirebaseFirestore.getInstance();
    }

    public <T extends BaseModel> void modificarValor(Map<String, String> rutaDocumento, Map<String, Object> valorModificar, final IProductionListener listener,
                                                     final String etiqueta, final Object clase) {
        if (!conexionInternet()) {
            fallaSincronica(listener, Constantes.Fallas.NO_INTERNET);
            return;
        }
        esperaRespuestaDB(constantesDB.documentos);
        String ruta = "";

        for (Map.Entry<String, String> coleccion : rutaDocumento.entrySet()) {
            ruta += coleccion.getKey() + "/";
            if (coleccion.getValue() == null)
                break;
            ruta += coleccion.getValue() + "/";
        }

        String rFinal = ruta.substring(0, ruta.length() - 1);
        DocumentReference listaDocumento = firebase.document(rFinal);
        listaDocumento.update(valorModificar).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                listener.exito(etiqueta, clase);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                listener.falla(etiqueta);
            }
        });
    }

    public <T extends BaseModel> void agregarDocumentoAutoID(Map<String, String> rutaDocumento, Map<String, Object> nuevoDoc, final IProductionListener listener,
                                                             final String etiqueta, final Object clase) {
        if (!conexionInternet()) {
            fallaSincronica(listener, Constantes.Fallas.NO_INTERNET);
            return;
        }
        esperaRespuestaDB(constantesDB.documentos);
        String ruta = "";

        for (Map.Entry<String, String> coleccion : rutaDocumento.entrySet()) {
            ruta += coleccion.getKey() + "/";
            if (coleccion.getValue() == null)
                break;
            ruta += coleccion.getValue() + "/";
        }

        String rFinal = ruta.substring(0, ruta.length() - 1);
        CollectionReference coleccion = firebase.collection(rFinal);
        coleccion.add(nuevoDoc).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                listener.exito(etiqueta, clase);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                listener.falla(etiqueta);
            }
        });
    }

    public <T extends BaseModel> void agregarDocumento(Map<String, String> rutaDocumento, Map<String, Object> nuevoDoc, final IProductionListener listener,
                                                       final String etiqueta, final Object clase) {
        if (!conexionInternet()) {
            fallaSincronica(listener, Constantes.Fallas.NO_INTERNET);
            return;
        }
        esperaRespuestaDB(constantesDB.documentos);
        String ruta = "";

        for (Map.Entry<String, String> coleccion : rutaDocumento.entrySet()) {
            ruta += coleccion.getKey() + "/";
            if (coleccion.getValue() == null)
                break;
            ruta += coleccion.getValue() + "/";
        }

        String rFinal = ruta.substring(0, ruta.length() - 1);
        DocumentReference listaDocumento = firebase.document(rFinal);
        listaDocumento.set(nuevoDoc).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                listener.exito(etiqueta, clase);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                listener.falla(etiqueta);
            }
        });
    }

    public <T extends BaseModel> void borrarDocumento(Map<String, String> rutaDocumento, final IProductionListener listener,
                                                      final String etiqueta, final Object clase) {
        if (!conexionInternet()) {
            fallaSincronica(listener, Constantes.Fallas.NO_INTERNET);
            return;
        }
        esperaRespuestaDB(constantesDB.documentos);
        String ruta = "";

        for (Map.Entry<String, String> coleccion : rutaDocumento.entrySet()) {
            ruta += coleccion.getKey() + "/";
            if (coleccion.getValue() == null)
                break;
            ruta += coleccion.getValue() + "/";
        }

        String rFinal = ruta.substring(0, ruta.length() - 1);
        DocumentReference listaDocumento = firebase.document(rFinal);
        listaDocumento.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                listener.exito(etiqueta, clase);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                listener.falla(etiqueta);
            }
        });
    }

    public <T extends BaseModel> void consultarDocumento(Map<String, String> rutaDocumento, final IProductionListener listener, final String etiqueta, final Class <T> clase) {

        if(!conexionInternet())
        {
            fallaSincronica(listener,Constantes.Fallas.NO_INTERNET);
            return;
        }

        esperaRespuestaDB(constantesDB.documentos);
        String ruta = "";

        for (Map.Entry<String, String> coleccion : rutaDocumento.entrySet()) {
            ruta += coleccion.getKey() + "/";
            if (coleccion.getValue() == null)
                break;
            ruta += coleccion.getValue() + "/";
        }

        String rFinal = ruta.substring(0, ruta.length() - 1);
        Log.e("URl",rFinal);
        DocumentReference listaDocumento = firebase.document(rFinal);
        listaDocumento.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists())
                {

                    //int rol = documentSnapshot.getValue(Constantes.Roles.CLIENTE);
                    T result = documentSnapshot.toObject(clase);
                    result.uidDocumento=documentSnapshot.getId();
                    listener.actualizacion(etiqueta,result);
                }
                Log.e("lo logramos","Dale :)");
                llego = true;
                return;
            }
        });
        listaDocumento.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful())
                {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists())
                    {
                        T result = document.toObject(clase);
                        result.uidDocumento=document.getId();
                        listener.exito(etiqueta,result);
                    }
                    Log.e("lo logramos","Dale :)");
                    llego = true;
                    return;
                }
                listener.falla(etiqueta);
                Log.e("Fallo", "ConsultarDocumento");
                llego = true;
            }
        });
    }

    public <T extends BaseModel> void consultarColeccion(Map<String, String> rutaColeccion, final IProductionListener listener, final String etiqueta, final Class <T> clase) {
        Log.e("rutaaa", rutaColeccion.toString());
        if(!conexionInternet())
        {
            fallaSincronica(listener,Constantes.Fallas.NO_INTERNET);
            return;
        }

        esperaRespuestaDB(constantesDB.colecciones);
        String ruta = "";

        for (Map.Entry<String, String> coleccion : rutaColeccion.entrySet()) {
            ruta += coleccion.getKey() + "/";
            if (coleccion.getValue() == null)
                break;
            ruta += coleccion.getValue() + "/";
        }
        String rFinal = ruta.substring(0, ruta.length() - 1);
        CollectionReference listaColeciones = firebase.collection(rFinal);
        //  Log.e("Firebase",rFinal);
        listaColeciones.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                ArrayList<T> list = new ArrayList<>();
                int count = 0;
                for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                    documentos.add(document.getId());
                    list.add(document.toObject(clase));
                    list.get(count).uidDocumento=document.getId();
                    count++;
                }

                listener.actualizacion(etiqueta,list);
                llego = true;
                return;
            }
        });
        firebase.collection(rFinal).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    ArrayList<T> list = new ArrayList<>();
                    int count = 0;
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        documentos.add(document.getId());
                        list.add(document.toObject(clase));
                        list.get(count).uidDocumento=document.getId();
                        count++;
                    }
                    //Log.e("FIREBASE",list.get(0).listaCampos.get(0));
                    listener.exito(etiqueta,list);
                    llego = true;
                    return;
                }
                listener.falla(etiqueta);
                Log.e("Fallo", "ConsultarColeccion");
                llego=true;
            }
        });

    }

    public void esperaRespuestaDB(final constantesDB constante) {

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (!llego) {
                        Thread.sleep(500);
                    }
                    llego = false;
                    switch (constante) {
                        case colecciones:
                            Log.e("esperarRespuesta", "FirebaseFireStoreDB");
                            break;
                        case documentos:

                        case campos:

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        hilo.start();
    }


    public <T extends BaseModel, K extends BaseModel> void traerLista(T objeto, IProductionListener listener,
                                                                      String etiqueta, Class<K> clase) {
        if(!conexionInternet())
        {
            fallaSincronica(listener,Constantes.Fallas.NO_INTERNET);
            return;
        }
        firebase.collection(objeto.listaColeccion.get(0)).document();
    }

}
