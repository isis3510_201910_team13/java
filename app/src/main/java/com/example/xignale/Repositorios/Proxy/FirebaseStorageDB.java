package com.example.xignale.Repositorios.Proxy;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Repositorios.EndPoint.IProductionListener;
import com.example.xignale.Repositorios.SharedPreferences.PreferencesManager;
import com.example.xignale.Utilidades.Constantes;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FirebaseStorageDB extends BaseProxy {

    private FirebaseStorage storage;
    private String TAG = "FirebaseStorageDB";
    private ArrayList<String> url = new ArrayList<String>();

    public FirebaseStorageDB(Context context) {
        super(context);
        this.context = context;
    }

    public void subirFotoPerfil(final Usuario user, Uri uri, final IProductionListener listener, final String etiqueta) {
        try {
            final FirebaseStorage storage = FirebaseStorage.getInstance();
            final StorageReference storageRef = storage.getReference();
            final StorageReference mountainsRef = storageRef.child("usuarios/" + user.uidDocumento +"/foto_perfil.jpeg");

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            UploadTask uploadTask = mountainsRef.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    fallaSincronica(listener, Constantes.Fallas.FALLO_SUBIENDO_FOTO);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    devolverFotoPerfil(user, listener, etiqueta);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void devolverFotoPerfil(final Usuario usuario, final IProductionListener listener, final String etiqueta){
        final FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageRef = storage.getReference();
        final StorageReference mountainsRef = storageRef.child("usuarios/" + usuario.uidDocumento +"/foto_perfil.jpeg");
        mountainsRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                usuario.setUrlFotoPerfil(uri.toString());
                PreferencesManager.setUser(usuario, context);
                exitoSincronico(listener,usuario,etiqueta);
//                Picasso.get().load(usuario.urlFotoPerfil)
//                        .error(R.drawable.ic_facebook).into(ivPrueba);
            }
        });
    }

    public void recuperarFotosCategorias(final IProductionListener listener, final String etiqueta)
    {

    }

    public void descargarFoto(Map<String,String> url, final IProductionListener listener, final String etiqueta)
    {
        String ruta = "";
        final FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        final StorageReference storageReference = firebaseStorage.getReference();

        for(Map.Entry<String, String> urlFoto:url.entrySet())
        {
            ruta += urlFoto.getKey() + "/";

            if(urlFoto.getValue() == null)
            {
                break;
            }

            ruta += urlFoto.getValue() + "/";
        }

        String rFinal = ruta.substring(0,ruta.length() - 1);

        final StorageReference descargarRef = storageReference.child(rFinal);
        descargarRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri)
            {
                String urlFoto = uri.toString();
                Log.e("URL_Storage",urlFoto);
                listener.exito(etiqueta,urlFoto);
            }
        });

    }

    public void descargarFotos(ArrayList<String> urlsFotos, final IProductionListener listener, final String etiqueta)
    {
        String ruta = "";
        final FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        final StorageReference storageReference = firebaseStorage.getReference();
        final ArrayList<String>  vamos = new ArrayList<String>();

        for(String urlss: urlsFotos)
        {
            Log.e("URL_DALE",urlss);
            Map<String, String> direccion = new HashMap<>();
            direccion.put(urlss, null);
            direccion.put(Constantes.Storage.APLICACION, Constantes.Storage.PROMOCIONES);

            for (Map.Entry<String, String> urlFoto : direccion.entrySet())
            {
                ruta += urlFoto.getKey() + "/";

                if (urlFoto.getValue() == null) {
                    break;
                }

                ruta += urlFoto.getValue() + "/";
            }

            String rFinal = ruta.substring(0, ruta.length() - 1);
            Log.e("FINALL",rFinal);
            final StorageReference descargarRef = storageReference.child(rFinal);
            descargarRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri)
                {
                    String urlFoto = uri.toString();
                    vamos.add(urlFoto);
                    Log.e("URL_Storage", vamos.get(0));
                    //Log.e("DALEEE", vamos.get(1));
                }
            });
            ruta = "";
            rFinal = "";
        }

        listener.exito(etiqueta, vamos);
    }

}


