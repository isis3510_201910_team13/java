package com.example.xignale.Repositorios.DAOS;

import java.util.List;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.xignale.Modelos.HistorialBusqueda;


@Dao

public interface HistoricoBusquedaDAO
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertHistorico(HistorialBusqueda historial);

    @Query("SELECT * FROM HistorialBusqueda")
    List<HistorialBusqueda> getHistorico();

    @Update
    void updateHistorico(HistorialBusqueda historial);

    @Delete
    void deleteHistorico(HistorialBusqueda historial);
}
