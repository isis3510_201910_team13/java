package com.example.xignale.Repositorios.Proxy;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.xignale.Repositorios.EndPoint.IProductionListener;

import org.json.JSONObject;

public class ProxyREST extends BaseProxy{

    private RequestQueue queue;

    public ProxyREST(Context context){
        super(context);
        queue = Volley.newRequestQueue(context);
    }

    public <T,K>void realizarSolicitud(T objeto, final IProductionListener listener, final String etiqueta, Class<K> clase, String ruta){

        class AccionesError implements Response.ErrorListener {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "Error");
                listener.falla(etiqueta);
            }
        }

        class AccionesOk implements Response.Listener<JSONObject>{

            @Override
            public void onResponse(JSONObject response) {
                Log.e("htttp", response.toString());
                listener.exito("Volley", response);
            }
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(ruta,null, new AccionesOk(),new AccionesError());
        queue.add(jsonRequest);
    }

}
