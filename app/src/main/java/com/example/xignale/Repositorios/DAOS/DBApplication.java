package com.example.xignale.Repositorios.DAOS;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.xignale.Modelos.HistorialBusqueda;


@Database(entities = {HistorialBusqueda.class}, version = 1, exportSchema = false)
public abstract class DBApplication extends RoomDatabase {

    public abstract HistoricoBusquedaDAO historicoDAO();

    private static DBApplication dbApplication;

    public static DBApplication newInstance(Context context){
        if (dbApplication!=null)
            return dbApplication;
        dbApplication = Room.databaseBuilder(context.getApplicationContext(), DBApplication.class, "dbMusa")
                .allowMainThreadQueries().build();
        return dbApplication;
    }

}
