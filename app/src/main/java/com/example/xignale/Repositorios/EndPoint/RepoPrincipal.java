package com.example.xignale.Repositorios.EndPoint;

import android.content.Context;

import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Repositorios.Proxy.FirebaseFireStoreDB;
import com.example.xignale.Utilidades.Constantes;

import java.util.Map;
import java.util.TreeMap;

public class RepoPrincipal
{
    private Context activity;
    private String TAG = "repoPrincipal";
    private FirebaseFireStoreDB firebase;

    public RepoPrincipal(Context activity)
    {
        this.activity = activity;
        this.firebase = new FirebaseFireStoreDB(activity);
    }

    public void agregarAlerta(String cliente, Alerta alerta, final IProductionListener listener, final String etiqueta)
    {
        TreeMap<String, String> coleccion = new TreeMap<>();
        coleccion.put(Constantes.Colecciones.ALERTAS,alerta.uidDocumento);
        Map<String,Object> alertaDoc = new TreeMap<>();
        alertaDoc.put("descripcion", alerta.getDescripcion());
        alertaDoc.put("idUsuario", cliente);
        alertaDoc.put("lugar", alerta.getLugar());
        alertaDoc.put("tipo", alerta.getTipo());
        firebase.agregarDocumento(coleccion,alertaDoc,listener,etiqueta,alerta);
    }

    public void cargarInfoPersonal(String usuario, final IProductionListener listener, final String etiqueta)
    {
        TreeMap<String, String> coleccion = new TreeMap<>();
        coleccion.put(Constantes.Colecciones.USUARIOS, usuario);
        firebase.consultarDocumento(coleccion, listener, etiqueta, Usuario.class);
    }
}
