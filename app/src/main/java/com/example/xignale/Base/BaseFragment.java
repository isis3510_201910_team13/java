package com.example.xignale.Base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xignale.Componentes.DialogoDosBotones;
import com.example.xignale.Componentes.DialogoUnBoton;

public class BaseFragment extends Fragment
{
    private BaseActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (BaseActivity) getActivity();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void mostrarDialogoDosBtn(int fondo, int titulo, int descripcion, int imagen, int btnGrande, int btnPequenio,
                                     DialogoDosBotones.DialogoAlertaListener listener)
    {
        activity.mostrarDialogoDosBtn(fondo, titulo, descripcion, imagen, btnGrande, btnPequenio, listener);
    }

    public void mostrarDialogoUnBtn(int fondo, int titulo, int descripcion, int imagen, int btnGrande,
                                    DialogoUnBoton.DialogoAlertaListener listener)
    {
        activity.mostrarDialogoUnBtn(fondo, titulo, descripcion, imagen, btnGrande, listener);
    }

    public void aparecerProgressBar()
    {
        activity.aparecerProgressBar();
    }

    public void desaparecerProgressBar()
    {
        activity.desaparecerProgressBar();
    }
}
