package com.example.xignale.Base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.xignale.Componentes.DialogoDosBotones;
import com.example.xignale.Componentes.ProgressBar;

import com.example.xignale.Componentes.DialogoUnBoton;

public class BaseActivity extends AppCompatActivity
{
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    private Snackbar snackbar;
    private boolean internetConnected=true;
    private ProgressBar progressBar;

    public void mostrarDialogoDosBtn(int fondo, int titulo, int desripcion, int imagen, int btnGrande, int btnPequenio,
                                     DialogoDosBotones.DialogoAlertaListener listener) {
        DialogoDosBotones dialogo = DialogoDosBotones.newInstance(fondo, titulo, desripcion, imagen, btnGrande, btnPequenio);
        dialogo.listener = listener;
        dialogo.show(getSupportFragmentManager(), "DialogoDosBtn");
    }

    public void mostrarDialogoUnBtn(int fondo, int titulo, int desripcion, int imagen, int btnGrande,
                                    DialogoUnBoton.DialogoAlertaListener listener) {
        DialogoUnBoton dialogo = DialogoUnBoton.newInstance(fondo, titulo, desripcion, imagen, btnGrande);
        dialogo.listener = listener;
        dialogo.show(getSupportFragmentManager(), "DialogoUnBtn");
    }

    public void desaparecerProgressBar() {
        if (progressBar == null) {
            Log.e("Error en progress bar", "No hay un Progress Bar que desaparecer");
            return;
        } else
            progressBar.dismiss();
    }

    public void aparecerProgressBar() {
        progressBar = new ProgressBar();
        progressBar.show(getSupportFragmentManager(), "Progress Bar");
    }

}
