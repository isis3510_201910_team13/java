package com.example.xignale.Componentes;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.xignale.R;

import com.example.xignale.Utilidades.Constantes;

public class DialogoUnBoton extends DialogFragment {

    private View fondo;
    private TextView tvTitulo;
    private TextView tvDescripcion;
    private String etiqueta;
    private ImageView ivIcono;
    private Button btnGrande;
    public DialogoAlertaListener listener;

    public interface DialogoAlertaListener {
        void botonGrande(String etiqueta);
    }

    public static DialogoUnBoton newInstance(int fondo, int titulo, int desripcion, int imagen, int btnGrande) {
        DialogoUnBoton insta = new DialogoUnBoton();
        Bundle bundle = new Bundle();
        bundle.putInt(Constantes.BundleKey.FONDO_KEY, fondo);
        bundle.putInt(Constantes.BundleKey.TITULO_KEY, titulo);
        bundle.putInt(Constantes.BundleKey.DESCRIPCION_KEY, desripcion);
        bundle.putInt(Constantes.BundleKey.IMAGEN_KEY, imagen);
        bundle.putInt(Constantes.BundleKey.BTN_GRANDE_KEY, btnGrande);
        insta.setArguments(bundle);
        return insta;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_generico_un_boton, null);

        builder.setView(v);
        fondo = v.findViewById(R.id.layoutUnBtn);
        tvTitulo = (TextView) v.findViewById(R.id.tvTituloDialogoUnBtn);
        tvDescripcion = (TextView) v.findViewById(R.id.tvDescripcionDialogoUnBtn);
        ivIcono = (ImageView) v.findViewById(R.id.ivIcDialogoUnBtn);
        btnGrande = (Button) v.findViewById(R.id.btnGrandeDialogoUnBtn);

        fondo.setBackgroundResource(getArguments().getInt(Constantes.BundleKey.FONDO_KEY));
        tvTitulo.setText(getArguments().getInt(Constantes.BundleKey.TITULO_KEY));
        tvDescripcion.setText(getArguments().getInt(Constantes.BundleKey.DESCRIPCION_KEY));
        ivIcono.setImageResource(getArguments().getInt(Constantes.BundleKey.IMAGEN_KEY));
        btnGrande.setText(getArguments().getInt(Constantes.BundleKey.BTN_GRANDE_KEY));
        btnGrande.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                        if (listener == null) {
                            Log.e("Error dialogo", "listener=null");
                            return;
                        }
                        listener.botonGrande(etiqueta);
                    }
                }
        );
        return builder.create();
    }
}
