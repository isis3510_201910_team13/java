package com.example.xignale.Componentes;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Save
{
    private Context TheThis;
    private String NameOfFolder = "/Xignale";
    private String NameOfFile = "imagen";

    public void SaveImage(Context context, Bitmap ImageToSave) {
        Log.e("save", context.toString());
        TheThis = context;
        if(isExternalStorageWritable() == true) {
            String file_path = Environment.getExternalStorageDirectory() + NameOfFolder;
            Log.e("saveDale", file_path);
            String CurrentDateAndTime = getCurrentDateAndTime();
            File dir = new File(file_path);


            if (!dir.exists()) {
                dir.mkdirs();
            }

            File file = new File(dir, NameOfFile + CurrentDateAndTime + ".jpg");
            Log.e("saveDos",file.toString());

            FileOutputStream fOut = null;
            try {
                Log.e("lucha",file.toString());
                fOut = new FileOutputStream(file);
                Log.e("trye",fOut.toString());
                Log.e("try", "paso 1");
                ImageToSave.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                Log.e("try", "paso 2");
                fOut.flush();
                Log.e("try", "paso 3");
                fOut.close();
                Log.e("try", "paso 4");
                MakeSureFileWasCreatedThenMakeAvabile(file);
                Log.e("try", "paso 5");
                AbleToSave();
                Log.e("try", "si");
            } catch (FileNotFoundException e) {
                UnableToSave();
                Log.e("catch", "no FileNotFoundException");

            } catch (IOException e) {
                UnableToSave();
                Log.e("catch", "no IOException");
            }
        }

    }
    private void MakeSureFileWasCreatedThenMakeAvabile(File file){
        MediaScannerConnection.scanFile(TheThis,
                new String[] { file.toString() } , null,
                new MediaScannerConnection.OnScanCompletedListener() {

                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
    }

    private String getCurrentDateAndTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-­ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    private void UnableToSave() {
        Toast.makeText(TheThis, "¡No se ha podido guardar la imagen!", Toast.LENGTH_LONG).show();
    }

    private void AbleToSave() {
        Toast.makeText(TheThis, "Imagen guardada en la galería.", Toast.LENGTH_LONG).show();
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}