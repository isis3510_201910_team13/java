package com.example.xignale.Componentes;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.xignale.R;

import com.example.xignale.Utilidades.Constantes;

public class DialogoDosBotones extends DialogFragment {
    private View fondo;
    private TextView tvTitulo;
    private TextView tvDescripcion;
    private String etiqueta;
    private ImageView ivIcono;
    private Button btnGrande;
    private Button btnPequenio;
    public DialogoAlertaListener listener;

    public interface DialogoAlertaListener {
        void botonGrande(String etiqueta);

        void botonPequenio(String etiqueta);
    }

    public static DialogoDosBotones newInstance(int fondo, int titulo, int desripcion, int imagen, int btnGrande, int btnPequenio) {
        DialogoDosBotones insta = new DialogoDosBotones();
        Bundle bundle = new Bundle();
        bundle.putInt(Constantes.BundleKey.FONDO_KEY, fondo);
        bundle.putInt(Constantes.BundleKey.TITULO_KEY, titulo);
        bundle.putInt(Constantes.BundleKey.DESCRIPCION_KEY, desripcion);
        bundle.putInt(Constantes.BundleKey.IMAGEN_KEY, imagen);
        bundle.putInt(Constantes.BundleKey.BTN_GRANDE_KEY, btnGrande);
        bundle.putInt(Constantes.BundleKey.BTN_PEQUENIO_KEY, btnPequenio);
        insta.setArguments(bundle);
        return insta;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_generico_dos_botones, null);

        builder.setView(v);

        fondo = v.findViewById(R.id.layoutDosBtn);
        tvTitulo = (TextView) v.findViewById(R.id.tvTituloDialogoDosBtn);
        tvDescripcion = (TextView) v.findViewById(R.id.tvDescripcionDialogoDosBtn);
        ivIcono = (ImageView) v.findViewById(R.id.ivIcDialogoDosBtn);
        btnGrande = (Button) v.findViewById(R.id.btnGrandeDialogoDosBtn);
        btnPequenio = (Button) v.findViewById(R.id.btnPequenioDialogoDosBtn);

        fondo.setBackgroundResource(getArguments().getInt(Constantes.BundleKey.FONDO_KEY));
        tvTitulo.setText(getArguments().getInt(Constantes.BundleKey.TITULO_KEY));
        tvDescripcion.setText(getArguments().getInt(Constantes.BundleKey.DESCRIPCION_KEY));
        ivIcono.setImageResource(getArguments().getInt(Constantes.BundleKey.IMAGEN_KEY));
        btnGrande.setText(getArguments().getInt(Constantes.BundleKey.BTN_GRANDE_KEY));
        btnPequenio.setText(getArguments().getInt(Constantes.BundleKey.BTN_PEQUENIO_KEY));
        btnGrande.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                        if (listener == null) {
                            Log.e("Error dialogo", "listener=null");
                            return;
                        }
                        listener.botonGrande(etiqueta);
                    }
                }
        );
        btnPequenio.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                        if (listener == null) {
                            Log.e("Error dialogo", "listener=null");
                            return;
                        }
                        listener.botonPequenio(etiqueta);
                    }
                }

        );

        return builder.create();
    }
}
