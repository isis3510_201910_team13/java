package com.example.xignale.Presentacion.Principal;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xignale.Base.BaseActivity;
import com.example.xignale.Base.BaseFragment;
import com.example.xignale.Componentes.Save;
import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Presentacion.Alerta.AlertaActivity;
import com.example.xignale.Presentacion.EscanearActivity;
import com.example.xignale.Presentacion.Principal.Implementacion.AlertaFragment;
import com.example.xignale.Presentacion.Principal.Implementacion.PerfilUsuarioFragment;
import com.example.xignale.Presentacion.Principal.Implementacion.PrincipalPresenter;
import com.example.xignale.Presentacion.Principal.Implementacion.TiendasFragment;
import com.example.xignale.Presentacion.Principal.Implementacion.UbicacionFragment;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalPresenter;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalView;
import com.example.xignale.R;
import com.example.xignale.Utilidades.Constantes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class PrincipalActivity extends BaseActivity
{
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    private UbicacionFragment ubicacionFragment;
    private PerfilUsuarioFragment perfilUsuarioFragment;
    private TiendasFragment tiendasFragment;
    private AlertaFragment alertaFragment;
    private BottomNavigationView bottomNavigationView;
    private BaseFragment fragment;
    private FragmentManager fragmentManager;
    private Alerta alerta;
    private Usuario usuario;
    private IPrincipalPresenter presenter;
    private Snackbar snackbar;
    private boolean internetConnected=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        inicializar();
        escuchadores();
    }

    protected void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
        revisarForm();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    private void inicializar() {
        bottomNavigationView = findViewById(R.id.bnv_principal);
        presenter = new PrincipalPresenter(this, new AccionesPrincipal());
        presenter.cargarURL();
        //ubicacionFragment = new UbicacionFragment();
        //ubicacionFragment.listener = new AccionesUbicacionFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.fl_principal, ubicacionFragment).commit();
    }

    private void escuchadores()
    {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_ubicacion:
                        bottomNavigationView.getMenu().findItem(R.id.navigation_ubicacion).setChecked(true);
                        aparecerProgressBar();
                        presenter.cargarURL();
                        desaparecerProgressBar();
                        break;

                    case R.id.navigation_alertas:
                        bottomNavigationView.getMenu().findItem(R.id.navigation_alertas).setChecked(true);
                        aparecerProgressBar();
                        alertaFragment = new AlertaFragment();
                        alertaFragment.listener = new AccionesAlertasFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.fl_principal, alertaFragment).commit();
                        getSupportFragmentManager().beginTransaction().addToBackStack(null);
                        desaparecerProgressBar();
                        break;

                    case R.id.navigation_compartir_ubicacion:
                        //Compartir ubicacion
                        bottomNavigationView.getMenu().findItem(R.id.navigation_compartir_ubicacion).setChecked(true);
                        //aparecerProgressBar();
                        //tiendasFragment = new TiendasFragment();
                        //tiendasFragment.listener = new AccionesTiendasFragment();
                        //getSupportFragmentManager().beginTransaction().replace(R.id.fl_principal, tiendasFragment).commit();
                        //desaparecerProgressBar();

                        Intent intent = new Intent(PrincipalActivity.this, EscanearActivity.class);
                        //intent.putExtra(Constantes.BundleKey.TIPO_ALERTA,tipoAlerta);
                        startActivity(intent);
                        //finish();
                        break;

                    case R.id.navigation_mi_perfil:
                        bottomNavigationView.getMenu().findItem(R.id.navigation_mi_perfil).setChecked(true);
                        aparecerProgressBar();
                        /*
                        if(FirebaseAuth.getInstance().getCurrentUser().getUid()!= null)
                        {

                            String cliente = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            Log.e("UI", cliente);
                            presenter.cargarInfoUsuario(cliente);
                            desaparecerProgressBar();
                        }
                        else
                        {
                        */
                            presenter.cargarInfoUsuario("pWZ3ca62VnW2eOom0M4PYEihKw42");
                            desaparecerProgressBar();
                        //}

                        break;
                }
                return false;
            }
        });
    }

    private class AccionesTiendasFragment implements TiendasFragment.tiendasFragmentListener
    {
        @Override
        public void escanearCodigo()
        {
            /*
            final Activity activity = PrincipalActivity.this;
            IntentIntegrator integrator = new IntentIntegrator(activity);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.setPrompt("Escanear Factura");
            integrator.setCameraId(0);
            integrator.setOrientationLocked(false);
            integrator.setBeepEnabled(true);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();
            int resultCode=0;
            int requestCode =0;


            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

            tiendasFragment = tiendasFragment.newInstance(getIntent().getStringExtra());
            perfilUsuarioFragment.listener = new AccionesPerfilUsuarioFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_principal, perfilUsuarioFragment).commit();
            desaparecerProgressBar();
            */

        }
    }

    private class AccionesPerfilUsuarioFragment implements PerfilUsuarioFragment.PerfilUsuarioFragmentListener {

        @Override
        public void guardarFotoLocal(Bitmap bitmap)
        {
            Save saveFile = new Save();
            Log.e("activity", bitmap.toString());
            saveFile.SaveImage(PrincipalActivity.this, bitmap);
        }
    }

    private class AccionesUbicacionFragment implements UbicacionFragment.UbicacionFragmentListener {


    }

    private class AccionesAlertasFragment implements AlertaFragment.AlertasFragmentListener
    {
        String tipoAlerta ="";
        @Override
        public void navegarDescripcionAlerta(String tipoAlerta)
        {
            Intent intent = new Intent(PrincipalActivity.this, AlertaActivity.class);
            intent.putExtra(Constantes.BundleKey.TIPO_ALERTA,tipoAlerta);
            startActivity(intent);
            //alertaFragment.resetBackground();
        }
    }

    /*
    private class AccionesDescripcionAlerta implements DescripcionAlertaFragment.DescripcionAlertaFragmentListener
    {
        @Override
        public void agregarAlerta()
        {
            aparecerProgressBar();
            String cliente = FirebaseAuth.getInstance().getCurrentUser().getUid();
            presenter.agregarAlerta(cliente,alerta);
        }
    }
    */

    private class AccionesPrincipal implements IPrincipalView
    {

        @Override
        public void datosUsuario(Usuario usuario)
        {
            PrincipalActivity.this.usuario = usuario;

            if(usuario.getNombres() == null) {
                usuario.setNombres("Vacio");
            }

            if(usuario.getApellidos() == null) {
                usuario.setApellidos("Vacio");
            }

            if(usuario.getCorreo() == null) {
                usuario.setCorreo("Vacio");
            }

            if(usuario.getCedula() == null) {
                usuario.setCedula("Vacio");
            }

            if(usuario.getCelular() == null) {
                usuario.setCelular("Vacio");
            }

            if(usuario.getEmpresaAsociada() == null) {
                usuario.setEmpresaAsociada("Vacio");
            }

            if(usuario.getUrlFotoPerfil() == null) {
                usuario.setUrlFotoPerfil("https://firebasestorage.googleapis.com/v0/b/xignal-ee4fa.appspot.com/o/waldoo.png?alt=media&token=15ed732b-e52d-4cc0-9764-6b37983367e2");
            }

            perfilUsuarioFragment = PerfilUsuarioFragment.newInstance(getIntent().getStringExtra(Constantes.BundleKey.IDENTIFICADOR_USUARIO),usuario.getNombres(), usuario.getApellidos(), usuario.getCorreo(), usuario.getCelular(), usuario.getCedula(), usuario.getEmpresaAsociada(), usuario.getUrlFotoPerfil());
            perfilUsuarioFragment = PerfilUsuarioFragment.newInstance(getIntent().getStringExtra(Constantes.BundleKey.IDENTIFICADOR_USUARIO),usuario.getNombres(), usuario.getApellidos(), usuario.getCorreo(), usuario.getCelular(), usuario.getCedula(), usuario.getEmpresaAsociada(), usuario.getUrlFotoPerfil());
            perfilUsuarioFragment.listener = new AccionesPerfilUsuarioFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_principal, perfilUsuarioFragment).commit();
            getSupportFragmentManager().beginTransaction().addToBackStack(null);
        }

        @Override
        public void pintarUrl(String urlPintar)
        {
            ubicacionFragment = UbicacionFragment.newInstance(urlPintar);
            ubicacionFragment.listener = new AccionesUbicacionFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_principal, ubicacionFragment).commit();
            getSupportFragmentManager().beginTransaction().addToBackStack(null);
        }
    }

    public void revisarForm(){
        final Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                String aResponse = message.getData().getString(Constantes.BundleKey.VERIFICAR_FORM);
                if (aResponse != null) {
                    /*
                    btnAnadirVan.setEnabled(true);
                    Drawable background = btnAnadirVan.getBackground();
                    if (background instanceof ShapeDrawable) {
                        ((ShapeDrawable) background).getPaint().setColor(getResources().getColor(R.color.color_negro_al_75));
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable) background).setColor(getResources().getColor(R.color.color_negro_al_75));
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable) background).setColor(getResources().getColor(R.color.color_negro_al_75));
                    }
                    return true;
                    */
                }
                String aResponses = message.getData().getString(Constantes.BundleKey.FORM_INCOMPLETO);
                if (aResponses != null) {
                    /*
                    btnAnadirVan.setEnabled(false);
                    Drawable background = btnAnadirVan.getBackground();
                    if (background instanceof ShapeDrawable) {
                        ((ShapeDrawable) background).getPaint().setColor(getResources().getColor(R.color.color_negro_al_20));
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable) background).setColor(getResources().getColor(R.color.color_negro_al_20));
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable) background).setColor(getResources().getColor(R.color.color_negro_al_20));
                    }
                    return true;
                    */
                }
                return false;
            }
        });
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true)
                {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (alerta!=null)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constantes.BundleKey.VERIFICAR_FORM, "activar");
                        Message msg = handler.obtainMessage();
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                    }
                    else
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constantes.BundleKey.FORM_INCOMPLETO, "desactivar");
                        Message msg = handler.obtainMessage();
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                    }
                }
            }
        });
        hilo.start();
    }

    /**
     *  Method to register runtime broadcast receiver to show snackbar alert for internet connection..
     */
    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    /**
     *  Runtime Broadcast receiver inner class to capture internet connectivity events
     */
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            setSnackbarMessage(status,false);
        }
    };

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }
    private void setSnackbarMessage(String status,boolean showBar) {
        String internetStatus="";
        if(status.equalsIgnoreCase("Wifi enabled")||status.equalsIgnoreCase("Mobile data enabled")){
            internetStatus="Internet Connected";
            snackbar = Snackbar
                    .make(bottomNavigationView, internetStatus, Snackbar.LENGTH_LONG);
        }else {
            internetStatus="Lost Internet Connection";
            snackbar = Snackbar
                    .make(bottomNavigationView, internetStatus, Snackbar.LENGTH_INDEFINITE)
                    .setAction("X", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            snackbar.dismiss();
                        }
                    });
        }
        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE);
        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        if(internetStatus.equalsIgnoreCase("Lost Internet Connection")){
            if(internetConnected){
                snackbar.show();
                internetConnected=false;
            }
        }else{
            if(!internetConnected){
                internetConnected=true;
                snackbar.show();
            }
        }
    }

}