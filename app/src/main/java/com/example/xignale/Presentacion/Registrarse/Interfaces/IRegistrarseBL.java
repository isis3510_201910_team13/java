package com.example.xignale.Presentacion.Registrarse.Interfaces;

import com.example.xignale.Base.IBaseBL;

public interface IRegistrarseBL extends IBaseBL
{
    void guardarUsuario(String apellidos, String cedula, String celular, String correo, String empresa, String nombres);
}
