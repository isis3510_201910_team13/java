package com.example.xignale.Presentacion.Principal.Implementacion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.example.xignale.R;

import com.example.xignale.Base.BaseFragment;
import com.example.xignale.Utilidades.Constantes;

public class TiendasFragment extends BaseFragment
{
    private View vista;
    private Button btnScan;
    private TextView txtResultScan, txtFormatScan;
    public tiendasFragmentListener listener;

    public interface tiendasFragmentListener
    {
        void escanearCodigo();
    }

    public static TiendasFragment newInstance (String requestCode, String resultCode)
    {
        TiendasFragment insta = new TiendasFragment();
        Bundle arg = new Bundle();
        arg.putString(Constantes.BundleKey.REQUEST_CODE, requestCode);
        arg.putString(Constantes.BundleKey.RESULT_CODE, resultCode);
        insta.setArguments(arg);
        return insta;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        //super.onCreateView(inflater, container, savedInstanceState);
        vista = inflater.inflate(R.layout.activity_escanner, container, false);
        inicializar();
        escuchadores();
        return vista;

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void inicializar()
    {
        btnScan = (Button) vista.findViewById(R.id.btn_scan);
        txtResultScan = (TextView) vista.findViewById(R.id.txt_content_scan);
        txtFormatScan = (TextView) vista.findViewById(R.id.txt_format_scan);
    }

    public void escuchadores()
    {
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             listener.escanearCodigo();
            }
        });
    }

}
