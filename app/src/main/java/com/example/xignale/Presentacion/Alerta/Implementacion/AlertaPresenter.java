package com.example.xignale.Presentacion.Alerta.Implementacion;

import android.support.v4.app.FragmentActivity;

import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaBL;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaListener;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaPresenter;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaView;

public class AlertaPresenter implements IAlertaPresenter
{
    private FragmentActivity context;
    private IAlertaView iAlertaView;
    private IAlertaBL iAlertaBL;

    public AlertaPresenter (FragmentActivity context, IAlertaView iAlertaView)
    {
        this.context = context;
        this.iAlertaView = iAlertaView;
        iAlertaBL = new AlertaBL(context, new AccionesListener());
    }

    @Override
    public void agregarAlerta(String cliente, Alerta alerta)
    {
        iAlertaBL.agregarAlerta(cliente, alerta);
    }

    private class AccionesListener implements IAlertaListener
    {
        @Override
        public void alertaAgregada() {
            iAlertaView.notificarUsuario();
        }
    }
}
