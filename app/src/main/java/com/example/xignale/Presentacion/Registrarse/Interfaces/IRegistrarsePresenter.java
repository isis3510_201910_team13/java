package com.example.xignale.Presentacion.Registrarse.Interfaces;

public interface IRegistrarsePresenter
{
    void guardarUsuario(String apellidos, String cedula, String celular, String correo, String empresa, String nombres);
}
