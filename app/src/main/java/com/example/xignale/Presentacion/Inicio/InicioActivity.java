package com.example.xignale.Presentacion.Inicio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.xignale.Presentacion.IniciarSesion.IniciarSesionActivity;
import com.example.xignale.Presentacion.Principal.PrincipalActivity;
import com.example.xignale.Presentacion.Registrarse.RegistrarseActivity;
import com.example.xignale.R;

import com.example.xignale.Base.BaseActivity;

public class InicioActivity extends BaseActivity
{
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    private Button btnIniciarSesion;
    private Button btnRegistrarse;
    private TextView tvOlvidarContrasenia;
    private static final String LOG_TAG = "CheckNetworkStatus";
    private Snackbar snackbar;
    private ConstraintLayout constraintLayout;
    private boolean internetConnected=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_inicio);
            constraintLayout=findViewById(R.id.constraintLayoutInicio);
            inicializar();
    }

    protected void onResume()
    {
        super.onResume();
        escuchadores();
        registerInternetCheckReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }
    /**
     *  Method to register runtime broadcast receiver to show snackbar alert for internet connection..
     */
    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    /**
     *  Runtime Broadcast receiver inner class to capture internet connectivity events
     */
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            setSnackbarMessage(status,false);
        }
    };

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }
    private void setSnackbarMessage(String status,boolean showBar) {
        String internetStatus="";
        if(status.equalsIgnoreCase("Wifi enabled")||status.equalsIgnoreCase("Mobile data enabled")){
            internetStatus="Internet Connected";
            snackbar = Snackbar
                    .make(constraintLayout, internetStatus, Snackbar.LENGTH_LONG);
        }else {
            internetStatus="Lost Internet Connection";
            snackbar = Snackbar
                    .make(constraintLayout, internetStatus, Snackbar.LENGTH_INDEFINITE)
                    .setAction("X", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            snackbar.dismiss();
                        }
                    });
        }
        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE);
        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        if(internetStatus.equalsIgnoreCase("Lost Internet Connection")){
            if(internetConnected){
                snackbar.show();
                internetConnected=false;
            }
        }else{
            if(!internetConnected){
                internetConnected=true;
                snackbar.show();
            }
        }
    }

    private void inicializar()
    {
        btnIniciarSesion = findViewById(R.id.btn_iniciar_sesion_inicio);
        btnRegistrarse = findViewById(R.id.btn_registrarse_inicio);
        tvOlvidarContrasenia = findViewById(R.id.tv_olvido_contrasenia_inicio);
    }

    private void escuchadores()
    {
        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navegarRegistrarse();
            }
        });

        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navegarIniciarSesion();
            }
        });

        tvOlvidarContrasenia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navegarOlvidarContrasenia();
            }
        });
    }

    private void navegarRegistrarse()
    {
        Intent intent = new Intent(InicioActivity.this, RegistrarseActivity.class);
        //intent.putExtra(Constantes.BundleKey.TIPO_SERVICIO, Constantes.Pedir.PEDIR_YA);
        startActivity(intent);
    }

    private void navegarIniciarSesion()
    {
        //Intent intent = new Intent(InicioActivity.this, PrincipalActivity.class);
        Intent intent = new Intent(InicioActivity.this,IniciarSesionActivity.class);
        startActivity(intent);
    }

    private void navegarOlvidarContrasenia()
    {

    }
}
