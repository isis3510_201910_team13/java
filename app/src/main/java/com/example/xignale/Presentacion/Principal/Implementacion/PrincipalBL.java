package com.example.xignale.Presentacion.Principal.Implementacion;

import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalBL;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalListener;
import com.example.xignale.Repositorios.EndPoint.RepoPrincipal;
import com.example.xignale.Utilidades.Constantes;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class PrincipalBL implements IPrincipalBL
{
    private IPrincipalListener listener;
    private FragmentActivity context;
    private RepoPrincipal repoPrincipal;
    private static final String TAG = "";
    private URL url;
    private String urlPintar;

    public PrincipalBL(FragmentActivity context, IPrincipalListener listener)
    {
        this.context = context;
        this.listener = listener;
        repoPrincipal = new RepoPrincipal(context);
        urlPintar = "";
        try {
            url = new URL("https://maps.mapwize.io/#/v/edificio_mario_laserna?k=510d3bdc50cdaddc&u=default_universe&l=es&z=18&embed=true&venueId=5c9554b51ddebd00160b38ae&organizationId=5c8facb1d925af0063c2c550");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void cargarInfoUsuario(String uiUsuario)
    {
        repoPrincipal.cargarInfoPersonal(uiUsuario,this,Constantes.Servicios.MOSTRAR_INFO_USUARIO);
    }

    @Override
    public void pintarURL()
    {
        try {
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            File httpCacheDir = new File(context.getCacheDir(), "http");
            Class.forName("android.net.http.HttpResponseCache")
                    .getMethod("install", File.class, long.class)
                    .invoke(null, httpCacheDir, httpCacheSize);
        } catch (Exception httpResponseCacheNotAvailable) {
            Log.d(TAG, "HTTP response cache is unavailable.");
        }

        urlPintar = url.toString();
        listener.pintarUrl(urlPintar);
    }

    @Override
    public void exito(String etiqueta, Object objeto)
    {
        switch (etiqueta)
        {
            case Constantes.Servicios.MOSTRAR_INFO_USUARIO:
              //  Log.e("exito", "voy");
                listener.datosPersonalesUsuario((Usuario)objeto);
                break;

        }
    }

    @Override
    public void falla(String etiqueta) {

    }

    @Override
    public void actualizacion(String etiqueta, Object objeto)
    {
        switch (etiqueta)
        {
             case Constantes.Servicios.MOSTRAR_INFO_USUARIO:
                listener.datosPersonalesUsuario((Usuario)objeto);
                break;
        }
    }
}
