package com.example.xignale.Presentacion.Principal.Implementacion;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.xignale.R;
import com.example.xignale.Base.BaseFragment;

public class DescripcionAlertaFragment extends BaseFragment
{
    private View vista;
    private Button btnAceptar;
    public DescripcionAlertaFragmentListener listener;

    public interface DescripcionAlertaFragmentListener
    {
        void agregarAlerta();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        vista = inflater.inflate(R.layout.activity_descripcion_alerta, container, false);
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        inicializar();
        escuchadores();
    }

    private void inicializar()
    {
        //btnAceptar = (Button) vista.findViewById(R.id.btn_cajero_alertas);
    }

    public void escuchadores()
    {

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.agregarAlerta();
            }
        });


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
