package com.example.xignale.Presentacion.Alerta;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.example.xignale.Componentes.DialogoUnBoton;
import com.example.xignale.Componentes.Save;
import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Presentacion.Alerta.Implementacion.AlertaDescripcionFragment;
import com.example.xignale.Presentacion.Alerta.Implementacion.AlertaPresenter;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaPresenter;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaView;
import com.example.xignale.Presentacion.EscanearActivity;
import com.example.xignale.Presentacion.Principal.Implementacion.AlertaFragment;
import com.example.xignale.Presentacion.Principal.Implementacion.PerfilUsuarioFragment;
import com.example.xignale.Presentacion.Principal.Implementacion.PrincipalPresenter;
import com.example.xignale.Presentacion.Principal.Implementacion.UbicacionFragment;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalPresenter;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalView;
import com.example.xignale.Presentacion.Principal.PrincipalActivity;
import com.example.xignale.R;
import com.example.xignale.Base.BaseActivity;
import com.example.xignale.Utilidades.Constantes;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Date;

public class AlertaActivity extends BaseActivity
{
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    private AlertaDescripcionFragment alertaDescripcionFragment;
    private BottomNavigationView bottomNavigationView;
    private UbicacionFragment ubicacionFragment;
    private PerfilUsuarioFragment perfilUsuarioFragment;
    private AlertaFragment alertaFragment;
    private IAlertaPresenter presenter;
    private Button btnAceptar;
    private EditText etDescripcion;
    private String descripcion;
    private Snackbar snackbar;
    private ConstraintLayout constraintLayout;
    private boolean internetConnected=true;
    private IPrincipalPresenter presenterPrincipal;
    private Alerta alerta;
    private DialogoUnBoton dialogoUnBoton;
    private DialogoUnBoton dialogoUnBotonAgregarDescripcion;

    // presenter.agregarAlerta("k4FiPBrs5O91nEcvrRB8",);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_alerta);
        constraintLayout=findViewById(R.id.constraintLayoutAlertas);
        inicializar();
        escuchadores();
    }

    protected void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    private void inicializar()
    {
        alertaDescripcionFragment = new AlertaDescripcionFragment();
        alertaDescripcionFragment.listener = new AccionesAlertasDescripcionFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_ayuda,alertaDescripcionFragment).commit();
        //bottomNavigationView = findViewById(R.id.bnv_descripcion_alerta);
        presenter = new AlertaPresenter(this, new AccionesAlerta());
        presenterPrincipal = new PrincipalPresenter(this,new AccionesPrincipal());
    }

    private void escuchadores()
    {
//        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//               // bottomNavigationView.getMenu().findItem(R.id.navigation_alertas).setChecked(true);
//                switch (item.getItemId()) {
//                    //bottomNavigationView.getMenu().findItem(R.id.navigation_alertas).setChecked(true);
//                    case R.id.navigation_alertas:
//                        bottomNavigationView.getMenu().findItem(R.id.navigation_alertas).setChecked(true);
//                        aparecerProgressBar();
//                        AlertaActivity.this.finish();
//                        //Intent intentAlertasFragment = new Intent(AlertaActivity.this, PrincipalActivity.class);
//                        //startActivity(intentAlertasFragment);
//                        alertaFragment = new AlertaFragment();
//                        alertaFragment.listener = new AccionesAlertasFragment();
//                        getSupportFragmentManager().beginTransaction().replace(R.id.fl_ayuda, alertaFragment).commit();
//                        desaparecerProgressBar();
//                        break;
//
//                    case R.id.navigation_ubicacion:
//                        bottomNavigationView.getMenu().findItem(R.id.navigation_ubicacion).setChecked(true);
//                        Intent intent = new Intent(AlertaActivity.this, PrincipalActivity.class);
//                        //intent.putExtra(Constantes.BundleKey.TIPO_ALERTA,tipoAlerta);
//                        startActivity(intent);
//                        //ubicacionFragment = new UbicacionFragment();
//                        //ubicacionFragment.listener = new AccionesUbicacionFragment();
//                        //getSupportFragmentManager().beginTransaction().replace(R.id.fl_principal, ubicacionFragment).commit();
//
//                        break;
//
//                    case R.id.navigation_compartir_ubicacion:
//                        //Compartir ubicacion
//                        bottomNavigationView.getMenu().findItem(R.id.navigation_compartir_ubicacion).setChecked(true);
//                        Intent intentDos = new Intent(AlertaActivity.this, EscanearActivity.class);
//                        startActivity(intentDos);
//                        break;
//
//                    case R.id.navigation_mi_perfil:
//                        bottomNavigationView.getMenu().findItem(R.id.navigation_mi_perfil).setChecked(true);
//                        aparecerProgressBar();
//                        //String cliente = FirebaseAuth.getInstance().getCurrentUser().getUid();
//                        presenterPrincipal.cargarInfoUsuario("k4FiPBrs5O91nEcvrRB8");
//                        desaparecerProgressBar();
//                        break;
//                }
//                return false;
//            }
//        });
    }

    private class AccionesAlertasDescripcionFragment implements AlertaDescripcionFragment.AlertaDescripcionFragmentListener
    {
        @Override
        public void agregarAlerta(String cliente, Alerta alerta)
        {
            aparecerProgressBar();
            //String cliente = FirebaseAuth.getInstance().getCurrentUser().getUid();
            alerta.setTipo(getIntent().getExtras().getString(Constantes.BundleKey.TIPO_ALERTA));
            presenter.agregarAlerta(cliente,alerta);
        }

        @Override
        public void noTenemosDescripcion()
        {
            dialogoUnBotonAgregarDescripcion = new DialogoUnBoton();
            dialogoUnBotonAgregarDescripcion.listener = new AccionesDialogoAgregarDescripcionAlerta();
            mostrarDialogoUnBtn(R.color.colorFondo,R.string.no_alerta_agregada,R.string.no_alerta_descripcion_agregada,R.drawable.ic_error_negro,R.string.boton_agregar_descripcion,dialogoUnBotonAgregarDescripcion.listener);
        }
    }

    private class AccionesPerfilUsuarioFragment implements PerfilUsuarioFragment.PerfilUsuarioFragmentListener {

        @Override
        public void guardarFotoLocal(Bitmap bitmap)
        {
            Save saveFile = new Save();
            Log.e("activity", bitmap.toString());
            saveFile.SaveImage(AlertaActivity.this, bitmap);
        }
    }

    private class AccionesUbicacionFragment implements UbicacionFragment.UbicacionFragmentListener {

    }

    private class AccionesAlertasFragment implements AlertaFragment.AlertasFragmentListener
    {
        String tipoAlerta ="";
        @Override
        public void navegarDescripcionAlerta(String tipoAlerta)
        {
            //Intent intent = new Intent(AlertaActivity.this, AlertaActivity.class);
            //intent.putExtra(Constantes.BundleKey.TIPO_ALERTA,tipoAlerta);
            //startActivity(intent);
        }
    }

    private class AccionesAlerta implements IAlertaView
    {
        @Override
        public void notificarUsuario()
        {
         desaparecerProgressBar();
         alerta = null;
         dialogoUnBoton = new DialogoUnBoton();
         dialogoUnBoton.listener = new AccionesDialogo();
         mostrarDialogoUnBtn(R.color.colorFondo,R.string.alerta_agregada,R.string.alerta_descripcion_agregada,R.drawable.ic_check_negro,R.string.boton_aceptar,dialogoUnBoton.listener);
        }
    }

    private class AccionesPrincipal implements IPrincipalView
    {
        @Override
        public void datosUsuario(Usuario usuario)
        {

        }

        @Override
        public void pintarUrl(String urlPintar) {

        }
    }

    private class AccionesDialogo implements DialogoUnBoton.DialogoAlertaListener
    {
        @Override
        public void botonGrande(String etiqueta)
        {
            AlertaActivity.this.finish();
            alertaFragment = new AlertaFragment();
            alertaFragment.listener = new AccionesAlertasFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_ayuda, alertaFragment).commit();
        }
    }

    private class AccionesDialogoAgregarDescripcionAlerta implements DialogoUnBoton.DialogoAlertaListener
    {

        @Override
        public void botonGrande(String etiqueta) {

        }
    }

    /**
     *  Method to register runtime broadcast receiver to show snackbar alert for internet connection..
     */
    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    /**
     *  Runtime Broadcast receiver inner class to capture internet connectivity events
     */
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            setSnackbarMessage(status,false);
        }
    };

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }
    private void setSnackbarMessage(String status,boolean showBar) {
        String internetStatus="";
        if(status.equalsIgnoreCase("Wifi enabled")||status.equalsIgnoreCase("Mobile data enabled")){
            internetStatus="Internet Connected";
            snackbar = Snackbar
                    .make(constraintLayout, internetStatus, Snackbar.LENGTH_LONG);
        }else {
            internetStatus="Lost Internet Connection";
            snackbar = Snackbar
                    .make(constraintLayout, internetStatus, Snackbar.LENGTH_INDEFINITE)
                    .setAction("X", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            snackbar.dismiss();
                        }
                    });
        }
        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE);
        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        if(internetStatus.equalsIgnoreCase("Lost Internet Connection")){
            if(internetConnected){
                snackbar.show();
                internetConnected=false;
            }
        }else{
            if(!internetConnected){
                internetConnected=true;
                snackbar.show();
            }
        }
    }
}
