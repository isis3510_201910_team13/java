package com.example.xignale.Presentacion.Registrarse;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xignale.Componentes.DialogoUnBoton;
import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Presentacion.IniciarSesion.IniciarSesionActivity;
import com.example.xignale.Presentacion.Inicio.InicioActivity;
import com.example.xignale.Presentacion.Principal.PrincipalActivity;
import com.example.xignale.Presentacion.Registrarse.Implementacion.RegistrarsePresenter;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarsePresenter;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarseView;
import com.example.xignale.R;
import com.example.xignale.Base.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Locale;

public class RegistrarseActivity extends BaseActivity
{
    private EditText nombres;
    private EditText apellidos;
    private EditText cedula;
    private EditText celular;
    private EditText empresa;
    private EditText email;
    private EditText password;
    private Button registerButton;
    private Button loginButton;
    private View vistaEncabezado;
    private TextView tvEncabezado;
    private FirebaseAuth firebaseAuth;
    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerIntent;
    private int btnSeleccionado;
    private Usuario usuario;
    private IRegistrarsePresenter presenter;
    private DialogoUnBoton dialogoUnBoton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        inicializar();
        escuchadores();

    }

    private void inicializar()
    {
        nombres = findViewById(R.id.et_nombres_registro);
        apellidos = findViewById(R.id.et_apellidos_registro);
        cedula = findViewById(R.id.et_cedula_registro);
        celular = findViewById(R.id.et_celular_registro);
        empresa = findViewById(R.id.et_empresa_registro);
        email = (EditText) findViewById(R.id.et_email_registro);
        password = (EditText) findViewById(R.id.et_contrasenia_registro);
        registerButton = (Button) findViewById(R.id.btn_registrarme_registro);
        loginButton = (Button) findViewById(R.id.btn_registrado_listo_registro);
        vistaEncabezado = (View) findViewById(R.id.encabezado_registro);
        tvEncabezado = vistaEncabezado.findViewById(R.id.tv_titulo_pantallas);
        tvEncabezado.setText(R.string.titulo_registrarse);
        presenter = new RegistrarsePresenter(this, new AccionesRegistrarse());


        firebaseAuth = FirebaseAuth.getInstance();

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches!=null) {
                    if (btnSeleccionado == 1)
                        nombres.setText(matches.get(0));
                    else if(btnSeleccionado == 2)
                        apellidos.setText(matches.get(0));
                    else if(btnSeleccionado == 3)
                        cedula.setText(matches.get(0));
                    else if(btnSeleccionado == 4)
                        celular.setText(matches.get(0));
                    else if(btnSeleccionado == 5)
                        empresa.setText(matches.get(0));
                    else if(btnSeleccionado == 6)
                        email.setText(matches.get(0));
                    else
                        password.setText(matches.get(0));
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });

        findViewById(R.id.btn_nombres_registro).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        nombres.setHint("");
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 1;
                        nombres.setText("");
                        nombres.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        findViewById(R.id.btn_apellidos_registro).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        apellidos.setHint("");
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 2;
                        apellidos.setText("");
                        apellidos.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        findViewById(R.id.btn_cedula_registro).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        cedula.setHint("");
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 3;
                        cedula.setText("");
                        cedula.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        findViewById(R.id.btn_celular_registro).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        celular.setHint("");
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 4;
                        celular.setText("");
                        celular.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        findViewById(R.id.btn_empresa_registro).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        empresa.setHint("");
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 5;
                        empresa.setText("");
                        empresa.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        findViewById(R.id.btn_correo_registro).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        email.setHint("");
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 6;
                        email.setText("");
                        email.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        findViewById(R.id.btn_contrasenia_registro).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 7;
                        password.setText("");
                        password.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });
    }

    private void escuchadores()
    {
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aparecerProgressBar();
                String nombresString = nombres.getText().toString();
                String apellidosString = apellidos.getText().toString();
                String cedulaString = cedula.getText().toString();
                String celularString = celular.getText().toString();
                String empresaString = empresa.getText().toString();
                String emailString = email.getText().toString();
                String parola = password.getText().toString();


                if(TextUtils.isEmpty(nombresString)){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(apellidosString)){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(cedulaString)){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(celularString)){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(empresaString)){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(emailString)){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(parola)){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show();
                }

                if(parola.length()<6){
                    desaparecerProgressBar();
                    Toast.makeText(getApplicationContext(),"La contraseña debe contener al menos 6 caracteres",Toast.LENGTH_SHORT).show();
                }

                firebaseAuth.createUserWithEmailAndPassword(emailString,parola)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    startActivity(new Intent(getApplicationContext(),PrincipalActivity.class));
                                    desaparecerProgressBar();
                                    finish();
                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"E-mail or password is wrong",Toast.LENGTH_LONG).show();
                                    desaparecerProgressBar();
                                }
                            }
                        });

                presenter.guardarUsuario(apellidosString, cedulaString, celularString, emailString, empresaString, nombresString);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),IniciarSesionActivity.class));
            }
        });

        //if(firebaseAuth.getCurrentUser()!=null){
        //    startActivity(new Intent(getApplicationContext(),PrincipalActivity.class));
        //}
    }

    private class AccionesRegistrarse implements IRegistrarseView
    {
        @Override
        public void usuarioNuevo()
        {
            dialogoUnBoton = new DialogoUnBoton();
            dialogoUnBoton.listener = new AccionesDialogo();
            mostrarDialogoUnBtn(R.color.colorFondo,R.string.alerta_agregada,R.string.alerta_descripcion_agregada,R.drawable.ic_check_negro,R.string.boton_aceptar,dialogoUnBoton.listener);
        }
    }

    private class AccionesDialogo implements DialogoUnBoton.DialogoAlertaListener
    {
        @Override
        public void botonGrande(String etiqueta)
        {
            RegistrarseActivity.this.finish();
            //alertaFragment = new AlertaFragment();
            //alertaFragment.listener = new AccionesAlertasFragment();
            //getSupportFragmentManager().beginTransaction().replace(R.id.fl_ayuda, alertaFragment).commit();
        }
    }


}
