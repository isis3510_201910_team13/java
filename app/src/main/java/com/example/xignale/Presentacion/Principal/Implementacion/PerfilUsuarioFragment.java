package com.example.xignale.Presentacion.Principal.Implementacion;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xignale.R;

import com.example.xignale.Base.BaseFragment;
import com.example.xignale.Utilidades.Constantes;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class PerfilUsuarioFragment extends BaseFragment
{
    private View vista;
    private CircleImageView civFotoUsuario;
    private EditText etNombres;
    private Button btNombres;
    private EditText etApellidos;
    private Button btApellidos;
    private EditText etCedula;
    private Button btCedula;
    private EditText etCelular;
    private Button btCelular;
    private EditText etCorreo;
    private Button btCorreo;
    private EditText etEmpresa;
    private Button btEmpresa;
    private Button btnEditar;
    public PerfilUsuarioFragmentListener listener;
    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerIntent;
    private int btnSeleccionado;
    private View vistaEncabezado;
    private TextView tvEncabezado;

    public interface PerfilUsuarioFragmentListener
    {
        void guardarFotoLocal(Bitmap bitmap);
    }

    public static PerfilUsuarioFragment newInstance(String uidUsuario, String nombres, String apellidos, String correo, String cedula, String celular, String empresaAsociada, String urlFoto)
    {
        PerfilUsuarioFragment insta = new PerfilUsuarioFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.BundleKey.NOMBRES_USUARIO, nombres);
        args.putString(Constantes.BundleKey.APELLIDOS_USUARIO, apellidos);
        args.putString(Constantes.BundleKey.CEDULA_USUARIO, cedula);
        args.putString(Constantes.BundleKey.CORREO_USUARIO, correo);
        args.putString(Constantes.BundleKey.CELULAR_USUARIO, celular);
        args.putString(Constantes.BundleKey.EMPRESA_USUARIO,empresaAsociada);
        args.putString(Constantes.BundleKey.FOTO_USUARIO, urlFoto);

        insta.setArguments(args);
        return insta;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        vista = inflater.inflate(R.layout.fragment_perfil_usuario, container, false);
        inicializar();
        escuchadores();
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void inicializar()
    {
        vistaEncabezado = (View) vista.findViewById(R.id.encabezado_fragment_perfil_usuario);
        tvEncabezado = vistaEncabezado.findViewById(R.id.tv_titulo_pantallas);
        tvEncabezado.setText(R.string.titulo_perfil_usuario);

        civFotoUsuario = (CircleImageView) vista.findViewById(R.id.civ_usuario_perfil_usuario);
        Picasso.get().load(getArguments().getString(Constantes.BundleKey.FOTO_USUARIO))
                .placeholder(R.drawable.ic_perfil_sin_foto)
                .error(R.drawable.ic_perfil_sin_foto)
                .into(civFotoUsuario);
        etNombres = (EditText) vista.findViewById(R.id.et_nombres_perfil_usuario);
        etNombres.setText(getArguments().getString(Constantes.BundleKey.NOMBRES_USUARIO));
        etNombres.setEnabled(false);
        etNombres.setHint(getArguments().getString(Constantes.BundleKey.NOMBRES_USUARIO));
        btNombres = vista.findViewById(R.id.btn_nombres_perfil_usuario);
        btNombres.setVisibility(View.GONE);
        etApellidos = (EditText) vista.findViewById(R.id.et_apellidos_perfil_usuario);
        etApellidos.setText(getArguments().getString(Constantes.BundleKey.APELLIDOS_USUARIO));
        etApellidos.setEnabled(false);
        etApellidos.setHint(getArguments().getString(Constantes.BundleKey.APELLIDOS_USUARIO));
        btApellidos = vista.findViewById(R.id.btn_apellidos_perfil_usuario);
        btApellidos.setVisibility(View.GONE);
        etCedula = (EditText) vista.findViewById(R.id.et_cedula_perfil_usuario);
        etCedula.setHint(getArguments().getString(Constantes.BundleKey.CEDULA_USUARIO));
        etCedula.setText(getArguments().getString(Constantes.BundleKey.CEDULA_USUARIO));
        etCedula.setEnabled(false);
        btCedula = vista.findViewById(R.id.btn_cedula_perfil_usuario);
        btCedula.setVisibility(View.GONE);
        etCelular = (EditText) vista.findViewById(R.id.et_celular_perfil_usuario);
        etCelular.setText(getArguments().getString(Constantes.BundleKey.CELULAR_USUARIO));
        etCelular.setHint(getArguments().getString(Constantes.BundleKey.CELULAR_USUARIO));
        etCelular.setEnabled(false);
        btCelular = vista.findViewById(R.id.btn_celular_perfil_usuario);
        btCelular.setVisibility(View.GONE);
        etCorreo = (EditText) vista.findViewById(R.id.et_correo_perfil_usuario);
        etCorreo.setText(getArguments().getString(Constantes.BundleKey.CORREO_USUARIO));
        etCorreo.setHint(getArguments().getString(Constantes.BundleKey.CORREO_USUARIO));
        etCorreo.setEnabled(false);
        btCorreo = vista.findViewById(R.id.btn_correo_perfil_usuario);
        btCorreo.setVisibility(View.GONE);
        etEmpresa = (EditText) vista.findViewById(R.id.et_empresa_perfil_usuario);
        etEmpresa.setText(getArguments().getString(Constantes.BundleKey.EMPRESA_USUARIO));
        etEmpresa.setHint(getArguments().getString(Constantes.BundleKey.EMPRESA_USUARIO));
        etEmpresa.setEnabled(false);
        btEmpresa = vista.findViewById(R.id.btn_empresa_perfil_usuario);
        btEmpresa.setVisibility(View.GONE);
        btnEditar = (Button) vista.findViewById(R.id.btn_perfil_usuario);

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches!=null) {
                    if (btnSeleccionado == 1)
                        etNombres.setText(matches.get(0));
                    else if (btnSeleccionado == 2)
                        etApellidos.setText(matches.get(0));
                    else if (btnSeleccionado == 3)
                        etCedula.setText(matches.get(0));
                    else if (btnSeleccionado == 4)
                        etCelular.setText(matches.get(0));
                    else if (btnSeleccionado == 5)
                        etCorreo.setText(matches.get(0));
                    else
                        etEmpresa.setText(matches.get(0));
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });

        vista.findViewById(R.id.btn_nombres_perfil_usuario).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 1;
                        etNombres.setText("");
                        etNombres.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        vista.findViewById(R.id.btn_apellidos_perfil_usuario).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 2;
                        etApellidos.setText("");
                        etApellidos.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        vista.findViewById(R.id.btn_cedula_perfil_usuario).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 3;
                        etCedula.setText("");
                        etCedula.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        vista.findViewById(R.id.btn_celular_perfil_usuario).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 4;
                        etCelular.setText("");
                        etCelular.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        vista.findViewById(R.id.btn_correo_perfil_usuario).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 5;
                        etCorreo.setText("");
                        etCorreo.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        vista.findViewById(R.id.btn_empresa_perfil_usuario).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 6;
                        etEmpresa.setText("");
                        etEmpresa.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });
    }

    public void escuchadores()
    {
        civFotoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                civFotoUsuario.buildDrawingCache();
                Bitmap bmap = civFotoUsuario.getDrawingCache();
                Log.e("fargment", bmap.toString());
                listener.guardarFotoLocal(bmap);
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNombres.setEnabled(true);
                etNombres.setHint(R.string.nombres_usuario_informe);
                btNombres.setVisibility(View.VISIBLE);
                etApellidos.setEnabled(true);
                etApellidos.setHint(R.string.apellidos_usuario_informe);
                btApellidos.setVisibility(View.VISIBLE);
                etCedula.setEnabled(true);
                etCedula.setHint(R.string.cedula_usuario_informe);
                btCedula.setVisibility(View.VISIBLE);
                etCelular.setEnabled(true);
                etCelular.setHint(R.string.celular_usuario_informe);
                btCelular.setVisibility(View.VISIBLE);
                etCorreo.setEnabled(true);
                etCorreo.setHint(R.string.correo_usuario_informe);
                btCorreo.setVisibility(View.VISIBLE);
                etEmpresa.setEnabled(true);
                etEmpresa.setHint(R.string.empresa_usuario_informe);
                btEmpresa.setVisibility(View.VISIBLE);
                btnEditar.setText(R.string.guardar);
                //listener.editarDatosUsuario();
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


}
