package com.example.xignale.Presentacion.Principal.InterfacesPrincipal;

import com.example.xignale.Modelos.Usuario;

public interface IPrincipalListener
{
    void datosPersonalesUsuario(Usuario usuario);

    void pintarUrl(String urlPintar);
}
