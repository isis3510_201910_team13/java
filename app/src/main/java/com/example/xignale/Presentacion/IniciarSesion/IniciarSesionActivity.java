package com.example.xignale.Presentacion.IniciarSesion;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xignale.Base.BaseActivity;
import com.example.xignale.Componentes.DialogoUnBoton;
import com.example.xignale.Presentacion.Alerta.AlertaActivity;
import com.example.xignale.Presentacion.Inicio.InicioActivity;
import com.example.xignale.Presentacion.Principal.PrincipalActivity;
import com.example.xignale.Presentacion.Registrarse.RegistrarseActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.example.xignale.R;

import java.util.ArrayList;
import java.util.Locale;

public class IniciarSesionActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener
{
    private EditText loginEmail;
    private EditText loginPassword;
    private Button loginButton;
    private Button newPassButton;
    private Button registroButton;
    private static final int RC_SIGN_IN = 9001;
    private SignInButton signInButton;
    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerIntent;
    private View vistaEncabezado;
    private TextView tvEncabezado;
    private int btnSeleccionado;
    private DialogoUnBoton dialogoUnBoton;
    private DialogoUnBoton dialogoUnBotonAgregarDescripcion;
    FirebaseAuth firebaseAuth;
    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);
        inicializar();

    }

    protected void onResume()
    {
        super.onResume();
        escuchadores();
    }

    private void inicializar()
    {
        loginEmail = (EditText) findViewById(R.id.et_email_iniciar_sesion);
        loginPassword = (EditText) findViewById(R.id.et_constrasenia_iniciar_sesion);
        loginButton = (Button) findViewById(R.id.btn_login_iniciar_sesion);
        registroButton = (Button) findViewById(R.id.btn_registro_iniciar_sesion);
        signInButton = (SignInButton) findViewById(R.id.sib_google_iniciar_sesion);
        newPassButton = (Button) findViewById(R.id.btn_olvide_contrasenia_iniciar_sesion);
        vistaEncabezado = (View) findViewById(R.id.encabezado_iniciar_sesion);
        tvEncabezado = vistaEncabezado.findViewById(R.id.tv_titulo_pantallas);
        tvEncabezado.setText(R.string.titulo_inicio_sesion);

        //firebaseAuth = FirebaseAuth.getInstance();

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches!=null) {
                    if (btnSeleccionado == 1)
                        loginEmail.setText(matches.get(0));
                    else
                        loginPassword.setText(matches.get(0));
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });

        findViewById(R.id.btn_correo_iniciar_sesion).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 1;
                        loginEmail.setText("");
                        loginEmail.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });

        findViewById(R.id.btn_contrasenia_iniciar_sesion).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnSeleccionado = 2;
                        loginPassword.setText("");
                        loginPassword.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });
    }

    private void escuchadores()
    {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Log.e("prueba1",loginEmail.getText().toString());
                if(loginEmail.getText().toString() != null || loginPassword.getText().toString() != null){
                    aparecerProgressBar();
                    startActivity(new Intent(getApplicationContext(),PrincipalActivity.class));
                    finish();
                    desaparecerProgressBar();
                }

                else if (loginEmail.getText().toString() == null || loginPassword.getText().toString() == null)
                {
                    aparecerProgressBar();
                    dialogoUnBotonAgregarDescripcion = new DialogoUnBoton();
                    dialogoUnBotonAgregarDescripcion.listener = new IniciarSesionActivity.AccionesDialogoAgregarDescripcionAlerta();
                    mostrarDialogoUnBtn(R.color.colorFondo,R.string.no_inicio_sesion,R.string.no_inicio_sesion_descripcion,R.drawable.ic_error_negro,R.string.boton_corregir_datos,dialogoUnBotonAgregarDescripcion.listener);
                    desaparecerProgressBar();
                }


            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(IniciarSesionActivity.this,this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
//                .build();

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                aparecerProgressBar();
                signIn();
            }
        });


        registroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                aparecerProgressBar();
                    startActivity(new Intent(getApplicationContext(),RegistrarseActivity.class));
                desaparecerProgressBar();
            }
        });


    }

    private void signIn() {
        Intent signIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signIntent,RC_SIGN_IN);
        desaparecerProgressBar();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()){
                GoogleSignInAccount account = result.getSignInAccount();
                authWithGoogle(account);
            }
        }
    }

    private void authWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(),null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    startActivity(new Intent(getApplicationContext(),PrincipalActivity.class));
                    finish();
                    desaparecerProgressBar();
                }
                else{
                    Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private class AccionesDialogoAgregarDescripcionAlerta implements DialogoUnBoton.DialogoAlertaListener
    {

        @Override
        public void botonGrande(String etiqueta) {

        }
    }

}
