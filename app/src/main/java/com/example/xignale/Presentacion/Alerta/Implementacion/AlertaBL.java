package com.example.xignale.Presentacion.Alerta.Implementacion;

import android.support.v4.app.FragmentActivity;

import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaBL;
import com.example.xignale.Presentacion.Alerta.Interfaces.IAlertaListener;
import com.example.xignale.Repositorios.EndPoint.RepoAlerta;
import com.example.xignale.Utilidades.Constantes;

public class AlertaBL implements IAlertaBL
{
    private IAlertaListener listener;
    private FragmentActivity context;
    private RepoAlerta repoAlerta;

    public AlertaBL(FragmentActivity context, IAlertaListener listener)
    {
        this.context = context;
        this.listener = listener;
        repoAlerta = new RepoAlerta(context);
    }

    @Override
    public void agregarAlerta(String cliente, Alerta alerta) {
        repoAlerta.agregarAlerta(cliente, alerta,this, Constantes.Servicios.AGREGAR_ALERTA);
    }

    @Override
    public void exito(String etiqueta, Object objeto)
    {
        switch (etiqueta)
        {
            case Constantes.Servicios.AGREGAR_ALERTA:
                listener.alertaAgregada();
                break;

        }
    }

    @Override
    public void falla(String etiqueta) {

    }

    @Override
    public void actualizacion(String etiqueta, Object objeto) {

    }


}
