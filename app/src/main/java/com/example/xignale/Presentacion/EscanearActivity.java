package com.example.xignale.Presentacion;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xignale.Modelos.Tiendas;
import com.example.xignale.Presentacion.Principal.Implementacion.RVAdapter;
import com.example.xignale.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class EscanearActivity extends AppCompatActivity {

    private Button btnScan;
    private RecyclerView rv;
    private View vistaEncabezado;
    private TextView tvEncabezado;
    private TextView txtResultScan, txtFormatScan;
    private List<Tiendas> persons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escanner);

        btnScan = (Button) this.findViewById(R.id.btn_scan);
        txtResultScan = (TextView) this.findViewById(R.id.txt_content_scan);
        txtFormatScan = (TextView) this.findViewById(R.id.txt_format_scan);
        vistaEncabezado = (View) findViewById(R.id.encabezado_escanear);
        tvEncabezado = vistaEncabezado.findViewById(R.id.tv_titulo_pantallas);
        tvEncabezado.setText("Nuestras tiendas");
        RecyclerView rv = (RecyclerView)findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        initializeData();
        RVAdapter adapter = new RVAdapter(persons);
        rv.setAdapter(adapter);


        final Activity activity = this;

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("Escanear Factura");
                integrator.setCameraId(0);
                integrator.setOrientationLocked(false);
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
            }
        });
    }

    private void initializeData(){
        persons = new ArrayList<>();
        //Bitmap obtener_imagen_amigo = get_imagen("https://maps.mapwize.io/#/f/p/edificio_mario_laserna/amigo/t/p/edificio_mario_laserna/ml_653?z=20.659");
        String obtener_imagen_amigo = "https://maps.mapwize.io/#/f/p/edificio_mario_laserna/amigo/t/p/edificio_mario_laserna/ml_653?z=20.659";
        String obtener_imagen_gratto = "https://maps.mapwize.io/#/f/p/edificio_mario_laserna/gratto/t/p/edificio_mario_laserna/ml_653?z=20.102";
        String obtener_imagen_cascabel ="https://maps.mapwize.io/#/f/p/edificio_mario_laserna/cascabel/t/p/edificio_mario_laserna/ml_653?z=19.93";
        persons.add(new Tiendas("Amigo", "Cafetería", R.mipmap.ic_amigo, obtener_imagen_amigo));
        persons.add(new Tiendas("Gratto", "Cafetería", R.mipmap.ic_gratto, obtener_imagen_gratto));
        persons.add(new Tiendas("Cascabel", "Pastelería", R.mipmap.ic_cascabel, obtener_imagen_cascabel));
    }

    private Bitmap get_imagen(String url) {
        Bitmap bm = null;
        try {
            URL _url = new URL(url);
            URLConnection con = _url.openConnection();
            con.connect();
            InputStream is = con.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {

        }
        return bm;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                Log.d("MainActivity", "Escaneo Cancelado");
                Toast.makeText(this, "Escaneao Cancelado", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("MainActivity", "Escaneo Realizado");
                Toast.makeText(this, result.getContents(), Toast.LENGTH_SHORT).show();
                txtResultScan.setText(result.getContents());
                txtFormatScan.setText(result.getFormatName());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            Log.d("MainActivity", "Escaneao Cancelado");
            Toast.makeText(this, "Escaneao Cancelado", Toast.LENGTH_SHORT).show();
        }

    }
}