package com.example.xignale.Presentacion.Alerta.Interfaces;

import com.example.xignale.Base.IBaseBL;
import com.example.xignale.Modelos.Alerta;

public interface IAlertaBL extends IBaseBL
{
    void agregarAlerta(String cliente, Alerta alerta);
}
