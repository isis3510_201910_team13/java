package com.example.xignale.Presentacion.Registrarse.Implementacion;

import android.support.v4.app.FragmentActivity;

import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarseBL;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarseListener;
import com.example.xignale.Repositorios.EndPoint.RepoLogin;
import com.example.xignale.Utilidades.Constantes;

public class RegistrarseBL implements IRegistrarseBL
{
    private IRegistrarseListener listener;
    private FragmentActivity context;
    private RepoLogin repoLogin;

    public RegistrarseBL(FragmentActivity context, IRegistrarseListener listener)
    {
        this.context =context;
        this.listener = listener;
        repoLogin = new RepoLogin(context);
    }

    @Override
    public void guardarUsuario(String apellidos, String cedula, String celular, String correo, String empresa, String nombres) {
        repoLogin.guardarUsuario(apellidos,cedula,celular,correo,empresa,nombres, this, Constantes.Servicios.AGREGAR_USUARIO);
    }

    @Override
    public void exito(String etiqueta, Object objeto) {
        switch (etiqueta)
        {
            case Constantes.Servicios.AGREGAR_USUARIO:
                listener.usuarioNuevo();
                break;

        }
    }

    @Override
    public void falla(String etiqueta) {

    }

    @Override
    public void actualizacion(String etiqueta, Object objeto)
    {
        switch (etiqueta)
        {
            case Constantes.Servicios.AGREGAR_USUARIO:
                listener.usuarioNuevo();
                break;

        }
    }
}
