package com.example.xignale.Presentacion.Principal.InterfacesPrincipal;

import com.example.xignale.Base.IBaseBL;
import com.example.xignale.Modelos.Alerta;

public interface IPrincipalBL extends IBaseBL
{
    void cargarInfoUsuario(String uiUsuario);

    void pintarURL();
}
