package com.example.xignale.Presentacion.Principal.Implementacion;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xignale.R;

import com.example.xignale.Base.BaseFragment;
import com.example.xignale.Utilidades.Constantes;

import org.w3c.dom.Text;

public class AlertaFragment extends BaseFragment
{
    private View vista;
    private ImageView ivCajero;
    private ImageView ivMantenimiento;
    private ImageView ivAcceso;
    private ImageView ivPerdido;
    private ImageView ivEvento;
    private ImageView ivCerrado;
    private ImageView ivSeguridad;
    private ImageView ivAmbulancia;
    private TextView tvCajero;
    private TextView tvMantenimiento;
    private TextView tvRestringido;
    private TextView tvPerdido;
    private TextView tvEvento;
    private TextView tvPuertaCerrada;
    private TextView tvSeguridad;
    private TextView tvAmbulancia;
    public AlertasFragmentListener listener;
    private String tipoAlerta;
    private int seleccionado;

    public interface AlertasFragmentListener
    {
        void navegarDescripcionAlerta(String tipoAlerta);
    }

    public static AlertaFragment newInstance(String tipo)
    {
        AlertaFragment insta = new AlertaFragment();
        Bundle arg = new Bundle();
        arg.putString(Constantes.BundleKey.TIPO_ALERTA,tipo);
        insta.setArguments(arg);
        return insta;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        //super.onCreateView(inflater, container, savedInstanceState);
        vista = inflater.inflate(R.layout.fragment_alertas, container, false);
        inicializar();
        escuchadores();
        return vista;

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void inicializar()
    {
        ivCajero = (ImageView) vista.findViewById(R.id.iv_cajero_alertas);
        tvCajero = (TextView) vista.findViewById(R.id.tv_cajero_alerta);
        ivMantenimiento = (ImageView) vista.findViewById(R.id.iv_mantenimiento_alertas);
        tvMantenimiento = (TextView) vista.findViewById(R.id.tv_mantenimiento_alerta);
        ivAcceso = (ImageView) vista.findViewById(R.id.iv_acceso_alertas);
        tvRestringido = (TextView) vista.findViewById(R.id.tv_restringido_alerta);
        ivPerdido = (ImageView) vista.findViewById(R.id.iv_perdido_alertas);
        tvPerdido = (TextView) vista.findViewById(R.id.tv_perdido_alerta);
        ivEvento = (ImageView) vista.findViewById(R.id.iv_evento_alertas);
        tvEvento = (TextView) vista.findViewById(R.id.tv_evento_alerta);
        ivCerrado = (ImageView) vista.findViewById(R.id.iv_cerrado_alertas);
        tvPuertaCerrada = (TextView) vista.findViewById(R.id.tv_puerta_cerrada_alerta);
        ivSeguridad = (ImageView) vista.findViewById(R.id.iv_seguridad_alertas);
        tvSeguridad = (TextView) vista.findViewById(R.id.tv_seguridad_alerta);
        ivAmbulancia = (ImageView) vista.findViewById(R.id.iv_ambulancia_alertas);
        tvAmbulancia = (TextView) vista.findViewById(R.id.tv_ambulancia_alerta);
    }

    public void escuchadores()
    {
        ivCajero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 1;
               tipoAlerta = tvCajero.getText().toString();
               listener.navegarDescripcionAlerta(tipoAlerta);

            }
        });

        ivCajero.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivCajero.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivCajero.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });

        ivMantenimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 2;
                tipoAlerta = tvMantenimiento.getText().toString();
                listener.navegarDescripcionAlerta(tipoAlerta);
            }
        });

        ivMantenimiento.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivMantenimiento.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivMantenimiento.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });

        ivAcceso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 3;
                tipoAlerta = tvRestringido.getText().toString();
                listener.navegarDescripcionAlerta(tipoAlerta);
            }
        });

        ivAcceso.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivAcceso.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivAcceso.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });

        ivPerdido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 4;
                tipoAlerta = tvPerdido.getText().toString();
                listener.navegarDescripcionAlerta(tipoAlerta);
            }
        });

        ivPerdido.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivPerdido.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivPerdido.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });

        ivEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 5;
                tipoAlerta = tvEvento.getText().toString();
                listener.navegarDescripcionAlerta(tipoAlerta);
            }
        });

        ivEvento.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivEvento.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivEvento.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });

        ivCerrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 6;
                tipoAlerta = tvPuertaCerrada.getText().toString();
                listener.navegarDescripcionAlerta(tipoAlerta);
            }
        });

        ivCerrado.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivCerrado.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivCerrado.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });

        ivSeguridad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 7;
                tipoAlerta = tvSeguridad.getText().toString();
                listener.navegarDescripcionAlerta(tipoAlerta);
            }
        });

        ivSeguridad.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivSeguridad.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivSeguridad.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });

        ivAmbulancia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado = 8;
                tipoAlerta = tvAmbulancia.getText().toString();
                listener.navegarDescripcionAlerta(tipoAlerta);
            }
        });

        ivAmbulancia.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        ivAmbulancia.setBackgroundResource(R.drawable.buttonshape);

                        break;
                    case MotionEvent.ACTION_DOWN:
                        ivAmbulancia.setBackgroundResource(R.drawable.bottonshapedos);

                        break;
                }

                return false;
            }
        });
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
    }

    public void resetBackground(){
        if(seleccionado == 1)
            ivCajero.setBackgroundResource(R.drawable.buttonshape);
        else if(seleccionado == 2)
            ivMantenimiento.setBackgroundResource(R.drawable.buttonshape);
        else if(seleccionado == 3)
            ivAcceso.setBackgroundResource(R.drawable.buttonshape);
        else if(seleccionado == 4)
            ivPerdido.setBackgroundResource(R.drawable.buttonshape);
        else if(seleccionado == 5)
            ivEvento.setBackgroundResource(R.drawable.buttonshape);
        else if(seleccionado == 6)
            ivCerrado.setBackgroundResource(R.drawable.buttonshape);
        else if(seleccionado == 7)
            ivSeguridad.setBackgroundResource(R.drawable.buttonshape);
        else
            ivAmbulancia.setBackgroundResource(R.drawable.buttonshape);
    }
}
