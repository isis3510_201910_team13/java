package com.example.xignale.Presentacion.Registrarse.Implementacion;

import android.support.v4.app.FragmentActivity;

import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarseBL;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarseListener;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarsePresenter;
import com.example.xignale.Presentacion.Registrarse.Interfaces.IRegistrarseView;

public class RegistrarsePresenter implements IRegistrarsePresenter
{
    private FragmentActivity context;
    private IRegistrarseView iRegistrarseView;
    private IRegistrarseBL iRegistrarseBL;

    public RegistrarsePresenter(FragmentActivity context, IRegistrarseView iRegistrarseView)
    {
        this.context = context;
        this.iRegistrarseView = iRegistrarseView;
        iRegistrarseBL = new RegistrarseBL(context,new AccionesListener());
    }

    @Override
    public void guardarUsuario(String apellidos, String cedula, String celular, String correo, String empresa, String nombres)
    {
        iRegistrarseBL.guardarUsuario(apellidos,cedula,celular,correo,empresa,nombres);
    }

    private class AccionesListener implements IRegistrarseListener
    {

        @Override
        public void usuarioNuevo() {

        }
    }
}
