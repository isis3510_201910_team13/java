package com.example.xignale.Presentacion.Principal.Implementacion;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.example.xignale.R;

import com.example.xignale.Base.BaseFragment;
import com.example.xignale.Utilidades.Constantes;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class UbicacionFragment extends BaseFragment
{
    private static final String TAG ="" ;
    private View vista;
    private WebView webView;
    private URL url;
    public UbicacionFragmentListener listener;

    public interface UbicacionFragmentListener
    {
        //void guardarCache();
    }

    public static UbicacionFragment newInstance(String urlMapa)
    {
        UbicacionFragment insta = new UbicacionFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.BundleKey.URL_MAPA, urlMapa);

        insta.setArguments(args);
        return insta;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        vista = inflater.inflate(R.layout.fragment_ubicacion, container, false);
        inicializar();
        escuchadores();
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void inicializar()
    {
        webView = (WebView) vista.findViewById(R.id.wv_mapa);

        /*
        try {
            url = new URL("https://maps.mapwize.io/#/v/edificio_mario_laserna?k=510d3bdc50cdaddc&u=default_universe&l=es&z=18&embed=true&venueId=5c9554b51ddebd00160b38ae&organizationId=5c8facb1d925af0063c2c550");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // url represents the website containing the content to place into the cache.
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

        long currentTime = System.currentTimeMillis();
        long expires = conn.getHeaderFieldDate("Expires", currentTime);
        long lastModified = conn.getHeaderFieldDate("Last-Modified", currentTime);

// lastUpdateTime represents when the cache was last updated.
        if (lastModified < lastUpdateTime) {
            // Skip update
        } else {
            // Parse update
            lastUpdateTime = lastModified;
        }
*/
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(getArguments().getString(Constantes.BundleKey.URL_MAPA));
        //listener.guardarCache();

    }

    public void escuchadores()
    {

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's state here
    }



}
