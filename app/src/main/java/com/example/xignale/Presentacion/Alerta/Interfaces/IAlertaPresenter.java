package com.example.xignale.Presentacion.Alerta.Interfaces;

import com.example.xignale.Modelos.Alerta;

public interface IAlertaPresenter
{
    void agregarAlerta(String cliente, Alerta alerta);
}
