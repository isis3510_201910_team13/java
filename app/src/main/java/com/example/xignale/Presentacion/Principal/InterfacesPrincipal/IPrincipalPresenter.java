package com.example.xignale.Presentacion.Principal.InterfacesPrincipal;

import com.example.xignale.Modelos.Alerta;

public interface IPrincipalPresenter
{
    void cargarInfoUsuario(String uiUsuario);

    void cargarURL();
}
