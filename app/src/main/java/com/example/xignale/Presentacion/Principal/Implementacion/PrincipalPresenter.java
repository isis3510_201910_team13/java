package com.example.xignale.Presentacion.Principal.Implementacion;

import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.xignale.Modelos.Alerta;
import com.example.xignale.Modelos.Usuario;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalBL;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalListener;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalPresenter;
import com.example.xignale.Presentacion.Principal.InterfacesPrincipal.IPrincipalView;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class PrincipalPresenter implements IPrincipalPresenter
{
    private FragmentActivity context;
    private IPrincipalView iPrincipalView;
    private IPrincipalBL iPrincipalBL;

    public PrincipalPresenter(FragmentActivity context, IPrincipalView iPrincipalView)
    {
        this.context = context;
        this.iPrincipalView = iPrincipalView;
        iPrincipalBL = new PrincipalBL(context, new AccionesListener());

    }
    @Override
    public void cargarInfoUsuario(String uiUsuario)
    {
        iPrincipalBL.cargarInfoUsuario(uiUsuario);
    }

    public void cargarURL()
    {
        iPrincipalBL.pintarURL();
    }


    private class AccionesListener implements IPrincipalListener {

        @Override
        public void datosPersonalesUsuario(Usuario usuario) {
            //Log.e("PresenterRespuesta", usuario.getNombres());
            iPrincipalView.datosUsuario(usuario);
        }

        @Override
        public void pintarUrl(String url)
        {
            iPrincipalView.pintarUrl(url);
        }
    }
}
