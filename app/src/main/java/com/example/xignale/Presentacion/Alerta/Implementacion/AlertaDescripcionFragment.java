package com.example.xignale.Presentacion.Alerta.Implementacion;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.xignale.Base.BaseFragment;
import com.example.xignale.Modelos.Alerta;
import com.example.xignale.R;
import com.google.firebase.Timestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class AlertaDescripcionFragment extends BaseFragment
{
    private View vista;
    private Button btnAceptar;
    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerIntent;
    private EditText etDescripcion;
    private Alerta alerta;
    private String descripcion;
    public AlertaDescripcionFragmentListener listener;

    public interface AlertaDescripcionFragmentListener
    {
        void agregarAlerta(String cliente, Alerta Alerta);
        void noTenemosDescripcion();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        vista = inflater.inflate(R.layout.fragment_descripcion_alerta, container, false);
        inicializar();
        escuchadores();
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void inicializar()
    {
        etDescripcion = vista.findViewById(R.id.et_descripcion_alerta);
        btnAceptar = vista.findViewById(R.id.btn_aceptar);
        alerta = new Alerta();
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches!=null)
                    etDescripcion.setText(matches.get(0));
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });


        vista.findViewById(R.id.btn_grabar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        speechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        etDescripcion.setText("");
                        etDescripcion.setHint("Escuchando...");
                        speechRecognizer.startListening(speechRecognizerIntent);
                        break;
                }

                return false;
            }
        });
    }

    public void escuchadores()
    {
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //String cliente = FirebaseAuth.getInstance().getCurrentUser().getUid();
                if(etDescripcion.getText().toString().isEmpty())
                {
                    listener.noTenemosDescripcion();
                }
                else {
                    descripcion = etDescripcion.getText().toString();
                    String cliente = "k4FiPBrs5O91nEcvrRB8";
                    alerta.setDescripcion(descripcion);
                    Date utilDate = new Date();
                    Timestamp fecha = new Timestamp(utilDate);
                    alerta.setFechaAlerta(fecha);
                    alerta.setLugar("ML");
                    listener.agregarAlerta(cliente, alerta);
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's state here
    }

    private void chechPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(!(ContextCompat.
                    checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)){
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getActivity().getPackageName()));
                startActivity(intent);
                getActivity().finish();
            }
        }
    }
}
