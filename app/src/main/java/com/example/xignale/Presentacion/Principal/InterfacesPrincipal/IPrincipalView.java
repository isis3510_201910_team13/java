package com.example.xignale.Presentacion.Principal.InterfacesPrincipal;

import com.example.xignale.Modelos.Usuario;

public interface IPrincipalView
{
    void datosUsuario(Usuario usuario);

    void pintarUrl(String urlPintar);
}
