package com.example.xignale.Modelos;

import com.example.xignale.Base.BaseModel;
import com.google.firebase.Timestamp;

public class Alerta extends BaseModel
{
    private String tipo;
    private String lugar;
    private String descripcion;
    private String uiUsuario;
    private Timestamp fechaAlerta;


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlerta() {
        return fechaAlerta;
    }

    public void setFechaAlerta(Timestamp fechaAlerta) {
        this.fechaAlerta = fechaAlerta;
    }

    public String getUiUsuario() {
        return uiUsuario;
    }

    public void setUiUsuario(String uiUsuario) {
        this.uiUsuario = uiUsuario;
    }
}
