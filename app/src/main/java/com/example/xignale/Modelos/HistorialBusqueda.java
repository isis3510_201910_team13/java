package com.example.xignale.Modelos;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
@Entity
public class HistorialBusqueda {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public String texto;

    @Override
    public String toString() {
        return (texto==null?super.toString():texto);
    }
}