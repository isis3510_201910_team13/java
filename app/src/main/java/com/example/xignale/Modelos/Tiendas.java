package com.example.xignale.Modelos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class Tiendas
{
    public String name;
    public String age;
    public int photoId;
    public String photoMapa;

    public Tiendas(String name, String age, int photoId, String photoMapa) {
        this.name = name;
        this.age = age;
        this.photoId = photoId;
        this.photoMapa = photoMapa;
    }

}

